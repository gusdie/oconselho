#!/bin/env node

function DaoUsuario() {
    this.add = function (usuar, fcb) {
        var clt = dbCon[0].collection("Usuario");
                var usuario = new Usuario();
                usuario.json(usuar);
                usuario.setAtivo(1);
                clt.insert(usuario, {
                    w: 1
                }, function (err, result) {
                    if (err) {
                        fcb(err);
                    } else {
                        fcb(result);
                    }
                });
    }

    this.recuperar = function (idUsuario, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        var clt = dbCon[0].collection("Usuario");

        clt.findOne({_id:ObjectId(idUsuario),ativo:1} ,function(err, result) {
            if(err){
                fcb(err,'erro no DAO');
            }
            else{
                if(result == null){
                    fcb('Usuario com esta id não existe.',null);
                }
                else{
                    fcb(null,result);
                }
            }
        });
    }

    this.recuperarLista = function (listaIds, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        var clt = dbCon[0].collection("Usuario");

        clt.find({_id:{
            $in: listaIds
        },ativo:1}).toArray(function(err, items) {
            fcb(err,items);
        });
    }

    this.atualizarMessagerIdUsuarioComId = function (mid,id, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        var clt = dbCon[0].collection("Usuario");

        clt.update({"_id":ObjectId(id)}, { $set:{
            "messagerId":mid}}, {w:1}, function(err, result) {

            fcb(err,result);
        });

    }

}

module.exports = DaoUsuario;