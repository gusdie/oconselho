#!/bin/env node

function DaoPolitico() {
    this.add = function (obj, fcb) {
        var clt = dbCon[0].collection("Politico");
        var objeto = new Politico();
        objeto.json(obj);
        objeto.setAtivo(1);
        clt.insert(objeto, {
            w: 1
        }, function (err, result) {
            if (err) {
                fcb(err);
            } else {
                fcb(result);
            }
        });
    }

    this.addLista = function (lista, fcb) {
        var clt = dbCon[0].collection("Politico");
        clt.insert(lista, {
            w: 1
        }, function (err, result) {
            if (err) {
                fcb(err);
            } else {
                fcb(result);
            }
        });
    }

    this.recuperar = function (id, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        var clt = dbCon[0].collection("Politico");

        clt.findOne({
            _id: ObjectId(id)
        }, function (err, result) {
            if (err) {
                fcb(err, 'erro no DAO');
            } else {
                if (result == null) {
                    fcb('Politico com este id não existe.', null);
                } else {
                    fcb(null, result);
                }
            }
        });
    }

    this.recuperarBusca = function (busca, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        var clt = dbCon[0].collection("Politico");

        if (!busca) {
            fcb('parametro busca faltante', null);
        }

        if (typeof busca == typeof 0) {
            clt.findOne({
                numero: busca,
                ativo: 1
            }, function (err, result) {
                if (err) {
                    fcb(err, null);
                } else {
                    if (result == null) {
                        fcb('Politico com este numero não existe.', null);
                    } else {
                        fcb(null, result);
                    }
                }
            });
        } else if (typeof busca == typeof "string") {
            clt.findOne({
                nome: busca,
                ativo: 1
            }, function (err, result) {
                if (err) {
                    fcb(err, null);
                } else {
                    if (result == null) {
                        fcb('Politico com este nome não existe.', null);
                    } else {
                        fcb(null, result);
                    }
                }
            });
        } else {
            fcb('parametro busca no formato errado', null);
        }

    }

    this.recuperarLista = function (listaIds, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        var clt = dbCon[0].collection("Politico");

        clt.find({
            _id: {
                $in: listaIds
            },
            ativo: 1
        }).toArray(function (err, items) {
            fcb(err, items);
        });
    }

    this.recuperarCargos = function (cargo, quantidade, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        if (!cargo || !quantidade) {
            fcb('dados faltantes para o método do DAO', null);
        }

        if (typeof quantidade != typeof 0 && quantidade != null) {
            fcb('quantidade em formato errado', null);
        }

        if (typeof cargo != typeof "string") {
            fcb('cargo em formato errado', null);
        }

        var clt = dbCon[0].collection("Politico");

        clt.find({
            ativo: '1',
            cargo: cargo
        }).limit(quantidade).sort({
            nome: 1
        }).toArray(function (err, items) {
            fcb(err, items);
        });
    }

}

module.exports = DaoPolitico;