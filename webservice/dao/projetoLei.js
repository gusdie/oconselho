#!/bin/env node

function DaoProjetoLei() {
    this.add = function (obj, fcb) {
        var clt = dbCon[0].collection("ProjetoLei");
                var objeto = new ProjetoLei();
                objeto.json(obj);
                objeto.setAtivo(1);
                clt.insert(objeto, {
                    w: 1
                }, function (err, result) {
                    if (err) {
                        fcb(err);
                    } else {
                        fcb(result);
                    }
                });
    }

    this.recuperar = function (id, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        var clt = dbCon[0].collection("ProjetoLei");

        clt.findOne({_id:ObjectId(id),ativo:1} ,function(err, result) {
            if(err){
                fcb(err,'erro no DAO');
            }
            else{
                if(result == null){
                    fcb('ProjetoLei com este id não existe.',null);
                }
                else{
                    fcb(null,result);
                }
            }
        });
    }

    this.recuperarLista = function (listaIds, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        var clt = dbCon[0].collection("ProjetoLei");

        clt.find({_id:{
            $in: listaIds
        },ativo:1}).toArray(function(err, items) {
            fcb(err,items);
        });
    }

}

module.exports = DaoProjetoLei;