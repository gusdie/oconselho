#!/bin/env node

function DaoBens() {
    this.add = function (obj, fcb) {
        var clt = dbCon[0].collection("Bens");
        var objeto = new Bens();
        objeto.json(obj);
        objeto.setAtivo(1);
        clt.insert(objeto, {
            w: 1
        }, function (err, result) {
            if (err) {
                fcb(err);
            } else {
                fcb(result);
            }
        });
    }

    this.recuperar = function (id, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        var clt = dbCon[0].collection("Bens");

        clt.findOne({
            _id: ObjectId(id)
        }, function (err, result) {
            if (err) {
                fcb(err, 'erro no DAO');
            } else {
                if (result == null) {
                    fcb('Bens com este id não existe.', null);
                } else {
                    fcb(null, result);
                }
            }
        });
    }

    this.addLista = function (lista, fcb) {
        var clt = dbCon[0].collection("Bens");
        clt.insert(lista, {
            w: 1
        }, function (err, result) {
            if (err) {
                fcb(err);
            } else {
                fcb(result);
            }
        });
    }

    this.recuperarLista = function (listaIds, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        var clt = dbCon[0].collection("Bens");

        clt.find({
            _id: {
                $in: listaIds
            }
        }).toArray(function (err, items) {
            fcb(err, items);
        });
    }

    this.total = function (idPolitico, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        if (!idPolitico) {
            fcb('dados faltantes para o método do DAO BENS', null);
        }

        if (typeof idPolitico != typeof "string") {
            fcb('idPolitico em formato errado', null);
        }

        var clt = dbCon[0].collection("Bens");

        clt.find({
            idTse: idPolitico
        }).toArray(function (err, items) {

            if (err) {
                fcb(err, null);
            }

            var soma = 0;

            console.log('AAAAA');
            console.log(items);
            console.log(idPolitico);

            var bem;

            for (var i = 0; i < items.length; i++) {
                bem = items[i];
                soma = soma + parseInt(bem.valor);
            }

            fcb(null, soma);
        });
    }

    this.totalArray = function (arrayIds, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        if (!arrayIds) {
            fcb('dados faltantes para o método do DAO BENS', null);
        }

        if (typeof arrayIds != typeof ['1', '2']) {
            fcb('arrayIds em formato errado', null);
        }

        var clt = dbCon[0].collection("Bens");

        clt.find({
            idPolitico: {
                $in: arrayIds
            }
        }).toArray(function (err, items) {

            if (err) {
                fcb(err, null);
            }

            var listaFinal = new Array();
            var bem = new Bens();

            var currentId = '';
            for (var i = 0; i < listaFinal.length; i++) {
                bem.json(items[i]);
                currentId = bem.getIdPolitico();
                for (var i = 0; i < listaFinal.length; i++) {
                    i
                }
            }

            fcb(null, soma);
        });
    }

}

module.exports = DaoBens;