#!/bin/env node

function DaoProposta() {
    this.add = function (obj, fcb) {
        var clt = dbCon[0].collection("Proposta");
        var objeto = new Proposta();
        objeto.json(obj);
        objeto.setAtivo(1);
        clt.insert(objeto, {
            w: 1
        }, function (err, result) {
            if (err) {
                fcb(err);
            } else {
                console.log('Entrei no método de insercao de nova proposta');

                var daoSeguindo = new DaoSeguindo();
                daoSeguindo.recuperarListaComPoliticoId(objeto.getIdPolitico(), function (err2, items) {
                    if (!err2) {
                        console.log('Entrei no recuperar lista de ids mensagens dos usuarios');
                        var daoPolitico = new DaoPolitico();
                        daoPolitico.recuperar(objeto.getIdPolitico(), function (err3, result3) {
                            if (!err3) {
                                console.log('Entrei no recuperar politico');
                                var inBot = require('inBot');
                                var politico = new Politico();
                                if (politico.json(result3)) {

                                    var lista = new Array();
                                    var seguindo;

                                    console.log(items);

                                    for (var i = 0; i < items.length; i++) {
                                        lista.push(items[i].idUsuario);
                                    }

                                    console.log('Entrei json politico');
                                    var texto = "Nova proposta de " + politico.getApelido() + " de " + objeto.getCategoria() + ": " + objeto.getDescricao();

                                    console.log(lista);
                                    console.log(texto);

                                    inBot.enviaMensagem('EAAaUNvdMNAQBACikgQjOJi7nmqnHdYt1FfS1RdAc6Y06OssdVPZCKI19uMIMZCIGCvxisv1B2xT3PTfDb5PJTPp8sortr9gRb36Bhssgn6be6VYkXb7CmtlgMyCYxt5mZA2Va7IuTMhwdjZAG9cKWkRjHyW4XRxCtFr3yCZB33gZDZD', lista, texto);
                                    fcb(result);
                                } else {
                                    console.log('ERRO -> result3 nao passou no .json de politico');
                                    console.log(result3);
                                    fcb(err3);
                                }

                            } else {
                                console.log('Erro ao carregar politico');
                                console.log(objeto.getIdPolitico());
                                console.log(items);
                                fcb(err3);
                                fcb(result3);
                            }
                        });
                    } else {
                        fcb(err2);
                    }
                });
            }
        });
    }

    this.recuperar = function (id, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        var clt = dbCon[0].collection("Proposta");

        clt.findOne({
            _id: ObjectId(id),
            ativo: 1
        }, function (err, result) {
            if (err) {
                fcb(err, 'erro no DAO');
            } else {
                if (result == null) {
                    fcb('Proposta com este id não existe.', null);
                } else {
                    fcb(null, result);
                }
            }
        });
    }

    this.recuperarVariasPolitico= function (idTse, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        var clt = dbCon[0].collection("Proposta");

        clt.find({
            idTse: idTse,
            'categoria': categoria5
        }).limit(quantidade).toArray(function (err5, items5) {

            if (err5) {
                fcb(err4, null);
            }

            propostasGerais.seguranca = items5;

            fcb(null, propostasGerais);

        });
    }

    this.addLista = function (lista, fcb) {
        var clt = dbCon[0].collection("Proposta");
        clt.insert(lista, {
            w: 1
        }, function (err, result) {
            if (err) {
                fcb(err);
            } else {
                fcb(result);
            }
        });
    }

    this.recuperarLista = function (listaIds, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        var clt = dbCon[0].collection("Proposta");

        clt.find({
            _id: {
                $in: listaIds
            },
            ativo: 1
        }).toArray(function (err, items) {
            fcb(err, items);
        });
    }

    this.recuperarVarias = function (idTse, fcb){
        var clt = dbCon[0].collection("Proposta");

        clt.find({idTse:idTse,ativo:1}).toArray(function(err, items) {
            fcb(err,items);
        });
    }

    this.gerais = function (idTse, quantidade, fcb) {
        // FIXME
        // verificar se esse usuario tem os campos obrigatorios preenchidos e tudo mais que for necessario antes de realmente mandar para o banco

        if (!idTse || !quantidade) {
            fcb('dados faltantes para o método do DAO BENS', null);
        }

        //if (typeof idTse != typeof "string") {
        //   fcb('idPoliidTsetico em formato errado', null);
        //}

        if (typeof quantidade != typeof 0) {
            fcb('quantidade em formato errado', null);
        }

        var clt = dbCon[0].collection("Bens");

        var categoria1 = 'SaÌ¼de';
        var categoria2 = 'EducaÌ¤Ì£o';
        var categoria3 = 'SeguranÌ¤a';
        var categoria4 = 'Mobilidade Urbana';
        var categoria5 = 'Cultura & Esporte';

        var propostasGerais = {};

        clt.find({
            idTse: idTse,
            'categoria': categoria1
        }).limit(quantidade).toArray(function (err, items) {

            if (err) {
                fcb(err, null);
            }

            propostasGerais.saude = items;

            clt.find({
                idTse: idTse,
                'categoria': categoria2
            }).limit(quantidade).toArray(function (err2, items2) {

                if (err2) {
                    fcb(err2, null);
                }

                propostasGerais.educacao = items2;

                clt.find({
                    idTse: idTse,
                    'categoria': categoria3
                }).limit(quantidade).toArray(function (err3, items3) {

                    if (err3) {
                        fcb(err3, null);
                    }

                    propostasGerais.seguranca = items3;

                    clt.find({
                        idTse: idTse,
                        'categoria': categoria4
                    }).limit(quantidade).toArray(function (err4, items4) {

                        if (err4) {
                            fcb(err4, null);
                        }

                        propostasGerais.seguranca = items4;

                        clt.find({
                            idTse: idTse,
                            'categoria': categoria5
                        }).limit(quantidade).toArray(function (err5, items5) {

                            if (err5) {
                                fcb(err4, null);
                            }

                            propostasGerais.seguranca = items5;

                            fcb(null, propostasGerais);

                        });

                    });

                });

            });

        });
    }

}

module.exports = DaoProposta;