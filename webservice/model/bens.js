#!/bin/env node

function Bens() {
    this.id = undefined;
    this.descricao = undefined;
    this.idPolitico = undefined;
    this.tipo = undefined;
    this.valor = undefined;
    this.ativo = undefined;

    // this.perfilSolicitante = new PerfilSolicitante();
    // this.perfilPrestadorServico = new PerfilPrestadorServico();

    this.jsonString = function (senha) {
        var s = this.senha;
        if (!senha) {
            this.setSenhaSemVerificar(undefined);
        }

        var j = JSON.stringify(this);

        if (!senha) {
            this.setSenhaSemVerificar(s);
        }

        return j;
    }

    this.json = function (j) {
        var ok = false;
        if (typeof j == typeof "x") {
            j = JSON.parse(j);
            ok = true;
        } else if (typeof j == typeof new Object) {
            ok = true;
        } else {
            ok = false;
        }
        if (ok && j) {
            // aqui dentro tem que ter todos os sets
            if (j.id == undefined || j.id == "undefined" || j.id == "" || j.id == 0 || j.id == "0") {
                this.setId(j._id); // id do mongo
            } else {
                this.setId(j.id);
            }

            this.setDescricao(j.descricao);
            this.setIdPolitico(j.idPolitico);
            this.setTipo(j.tipo);
            this.setValor(j.valor);
            if(j.ativo){
                this.setAtivo(j.ativo);
            }

            // this.setPerfilSolicitante(j.perfilSolicitante);
            // this.setPerfilPrestadorServico(j.perfilPrestadorServico);

            return true;
        } else {
            return false;
        }
    }

    this.setId = function (id) {
        // verificar se eh um id valido (o id do mongo)
        this.id = id;
        return true;
    }

    this.getId = function () {
        return this.id;
    }

    this.setDescricao = function (descricao){
        this.descricao = descricao;
        return true;
    }

    this.getDescricao = function (){
        return this.descricao;
    }

    this.setIdPolitico = function (idPolitico){
        this.idPolitico = idPolitico;
        return true;
    }

    this.getIdPolitico = function (){
        return this.idPolitico;
    }

    this.setTipo = function (tipo){
        this.tipo = tipo;
        return true;
    }

    this.getTipo = function (){
        return this.tipo;
    }

    this.setValor = function (valor){
        this.valor = valor;
        return true;
    }

    this.getValor = function (){
        return this.valor;
    }

    this.setAtivo = function (ativo){
        this.ativo = ativo;
        return true;
    }

    this.getAtivo = function (){
        return this.ativo;
    }
}

module.exports = Bens;