#!/bin/env node

function Partido() {
    this.id = undefined;
    this.nome = undefined;
    this.sigla = undefined;
    this.numero = undefined;
    this.ativo = undefined;

    // this.perfilSolicitante = new PerfilSolicitante();
    // this.perfilPrestadorServico = new PerfilPrestadorServico();

    this.jsonString = function (senha) {
        var s = this.senha;
        if (!senha) {
            this.setSenhaSemVerificar(undefined);
        }

        var j = JSON.stringify(this);

        if (!senha) {
            this.setSenhaSemVerificar(s);
        }

        return j;
    }

    this.json = function (j) {
        var ok = false;
        if (typeof j == typeof "x") {
            j = JSON.parse(j);
            ok = true;
        } else if (typeof j == typeof new Object) {
            ok = true;
        } else {
            ok = false;
        }
        if (ok && j) {
            // aqui dentro tem que ter todos os sets
            if (j.id == undefined || j.id == "undefined" || j.id == "" || j.id == 0 || j.id == "0") {
                this.setId(j._id); // id do mongo
            } else {
                this.setId(j.id);
            }

            this.setNome(j.nome);
            this.setSigla(j.sigla);
            this.setNumero(j.numero);
            if(j.ativo){
                this.setAtivo(j.ativo);
            }

            // this.setPerfilSolicitante(j.perfilSolicitante);
            // this.setPerfilPrestadorServico(j.perfilPrestadorServico);

            return true;
        } else {
            return false;
        }
    }

    this.setId = function (id) {
        // verificar se eh um id valido (o id do mongo)
        this.id = id;
        return true;
    }

    this.getId = function () {
        return this.id;
    }

    this.setNome = function (nome){
        this.nome = nome;
        return true;
    }

    this.getNome = function (){
        return this.nome;
    }

    this.setNumero = function (numero){
        this.numero = numero;
        return true;
    }

    this.getNumero = function (){
        return this.numero;
    }

    this.setSigla = function (sigla){
        this.sigla = sigla;
        return true;
    }

    this.getSigla = function (){
        return this.sigla;
    }

    this.setAtivo = function (ativo){
        this.ativo = ativo;
        return true;
    }

    this.getAtivo = function (){
        return this.ativo;
    }
}

module.exports = Partido;