#!/bin/env node

function Politico() {
    this.id = undefined;
    this.idTse = undefined;
    this.nome = undefined;
    this.apelido = undefined;
    this.ativo = undefined;
    this.candidato = undefined;
    this.cargo = undefined;
    this.coligacao = undefined;
    this.composicaoColigacao = undefined;
    this.cpf = undefined;
    this.eleito = undefined;
    this.email = undefined;
    this.estadoCivil = undefined;
    this.idPartido = undefined;
    this.idPolitico = undefined;
    this.imagem = undefined;
    this.instrucao = undefined;
    this.linkFace = undefined;
    this.linkTwitter = undefined;
    this.nacionalidade = undefined;
    this.nacionalidadeCidade = undefined;
    this.nacionalidadeEstado = undefined;
    this.nascimento = undefined;
    this.numero = undefined;
    this.ocupacao = undefined;
    this.protocolo = undefined;
    this.raca = undefined;
    this.sexo = undefined;
    this.viceId = undefined;
    this.idade = undefined;

    // this.perfilSolicitante = new PerfilSolicitante();
    // this.perfilPrestadorServico = new PerfilPrestadorServico();

    this.jsonString = function (senha) {
        var s = this.senha;
        if (!senha) {
            this.setSenhaSemVerificar(undefined);
        }

        var j = JSON.stringify(this);

        if (!senha) {
            this.setSenhaSemVerificar(s);
        }

        return j;
    }

    this.json = function (j) {
        var ok = false;
        if (typeof j == typeof "x") {
            j = JSON.parse(j);
            ok = true;
        } else if (typeof j == typeof new Object) {
            ok = true;
        } else {
            ok = false;
        }
        if (ok && j) {
            // aqui dentro tem que ter todos os sets
            if (j.id == undefined || j.id == "undefined" || j.id == "" || j.id == 0 || j.id == "0") {
                this.setId(j._id); // id do mongo
            } else {
                this.setId(j.id);
            }

            this.setNome(j.nome);
            if (j.ativo) {
                this.setAtivo(j.ativo);
            }
            this.setApelido(j.apelido);
            this.setIdTse(j.idTse);
            this.setAtivo(j.ativo);
            this.setCandidato(j.candidato);
            this.setCargo(j.cargo);
            this.setColigacao(j.coligacao);
            this.setComposicaoColigacao(j.composicaoColigacao);
            this.setCpf(j.cpf);
            this.setEleito(j.eleito);
            this.setEmail(j.email);
            this.setEstadoCivil(j.estadoCivil);
            this.setIdPartido(j.idPartido);
            this.setIdPolitico(j.idPolitico);
            this.setImagem(j.imagem);
            this.setInstrucao(j.instrucao);
            this.setLinkFace(j.linkFace);
            this.setLinkTwitter(j.linkTwitter);
            this.setNacionalidade(j.nacionalidade);
            this.setNacionalidadeCidade(j.nacionalidadeCidade);
            this.setNacionalidadeEstado(j.nacionalidadeEstado);
            this.setNascimento(j.nascimento);
            this.setNumero(j.numero);
            this.setOcupacao(j.ocupacao);
            this.setProtocolo(j.protocolo);
            this.setRaca(j.raca);
            this.setSexo(j.sexo);
            this.setViceId(j.viceId);


            // this.setPerfilSolicitante(j.perfilSolicitante);
            // this.setPerfilPrestadorServico(j.perfilPrestadorServico);

            return true;
        } else {
            return false;
        }
    }

    this.setId = function (id) {
        // verificar se eh um id valido (o id do mongo)
        this.id = id;
        return true;
    }

    this.getId = function () {
        return this.id;
    }

    this.setIdTse = function (idTse) {
        // verificar se eh um id valido (o id do mongo)
        this.idTse = idTse;
        return true;
    }

    this.getIdTse = function () {
        return this.idTse;
    }

    this.setNome = function (nome) {
        this.nome = nome;
        return true;
    }

    this.getNome = function () {
        return this.nome;
    }

    this.setApelido = function (apelido) {
        this.apelido = apelido;
        return true;
    }

    this.getApelido = function () {
        return this.apelido;
    }

    this.setAtivo = function (ativo) {
        this.ativo = ativo;
        return true;
    }

    this.getAtivo = function () {
        return this.ativo;
    }

    this.setCandidato = function (candidato) {
        this.candidato = candidato;
        return true;
    }

    this.getCandidato = function () {
        return this.candidato;
    }

    this.setCargo = function (cargo) {
        this.cargo = cargo;
        return true;
    }

    this.getCargo = function () {
        return this.cargo;
    }

    this.setColigacao = function (coligacao) {
        this.coligacao = coligacao;
        return true;
    }

    this.getColigacao = function () {
        return this.coligacao;
    }

    this.setComposicaoColigacao = function (composicaoColigacao) {
        this.composicaoColigacao = composicaoColigacao;
        return true;
    }

    this.getComposicaoColigacao = function () {
        return this.composicaoColigacao;
    }

    this.setCpf = function (cpf) {
        this.cpf = cpf;
        return true;
    }

    this.getCpf = function () {
        return this.cpf;
    }

    this.setEleito = function (eleito) {
        this.eleito = eleito;
        return true;
    }

    this.getEleito = function () {
        return this.eleito;
    }

    this.setEmail = function (email) {
        this.email = email;
        return true;
    }

    this.getEmail = function () {
        return this.email;
    }

    this.setEstadoCivil = function (estadoCivil) {
        this.estadoCivil = estadoCivil;
        return true;
    }

    this.getEstadoCivil = function () {
        return this.estadoCivil;
    }

    this.setIdPartido = function (idPartido) {
        this.idPartido = idPartido;
        return true;
    }

    this.getIdPartido = function () {
        return this.idPartido;
    }

    this.setIdPolitico = function (idPolitico) {
        this.idPolitico = idPolitico;
        return true;
    }

    this.getIdPolitico = function () {
        return this.idPolitico;
    }

    this.setImagem = function (imagem) {
        this.imagem = imagem;
        return true;
    }

    this.getImagem = function () {
        return this.imagem;
    }

    this.setInstrucao = function (instrucao) {
        this.instrucao = instrucao;
        return true;
    }

    this.getInstrucao = function () {
        return this.instrucao;
    }

    this.setLinkFace = function (linkFace) {
        this.linkFace = linkFace;
        return true;
    }

    this.getLinkFace = function () {
        return this.linkFace;
    }

    this.setLinkTwitter = function (linkTwitter) {
        this.linkTwitter = linkTwitter;
        return true;
    }

    this.getLinkTwitter = function () {
        return this.linkTwitter;
    }

    this.setNacionalidade = function (nacionalidade) {
        this.nacionalidade = nacionalidade;
        return true;
    }

    this.getNacionalidade = function () {
        return this.nacionalidade;
    }

    this.setNacionalidadeCidade = function (nacionalidadeCidade) {
        this.nacionalidadeCidade = nacionalidadeCidade;
        return true;
    }

    this.getNacionalidadeCidade = function () {
        return this.nacionalidadeCidade;
    }

    this.setNacionalidadeEstado = function (nacionalidadeEstado) {
        this.nacionalidadeEstado = nacionalidadeEstado;
        return true;
    }

    this.getNacionalidadeEstado = function () {
        return this.nacionalidadeEstado;
    }

    this.setNascimento = function (nascimento) {
        this.nascimento = nascimento;
        return true;
    }

    this.getNascimento = function () {
        return this.nascimento;
    }

    this.setNumero = function (numero) {
        this.numero = numero;
        return true;
    }

    this.getNumero = function () {
        return this.numero;
    }

    this.setOcupacao = function (ocupacao) {
        this.ocupacao = ocupacao;
        return true;
    }

    this.getOcupacao = function () {
        return this.ocupacao;
    }

    this.setProtocolo = function (protocolo) {
        this.protocolo = protocolo;
        return true;
    }

    this.getProtocolo = function () {
        return this.protocolo;
    }

    this.setRaca = function (raca) {
        this.raca = raca;
        return true;
    }

    this.getRaca = function () {
        return this.raca;
    }

    this.setSexo = function (sexo) {
        this.sexo = sexo;
        return true;
    }

    this.getSexo = function () {
        return this.sexo;
    }

    this.setViceId = function (viceId) {
        this.viceId = viceId;
        return true;
    }

    this.getViceId = function () {
        return this.viceId;
    }

    this.setIdade = function (idade) {
        this.idade = idade;
        return true;
    }

    this.getIdade = function () {
        return this.idade;
    }
}


module.exports = Politico;