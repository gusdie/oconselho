#!/bin/env node

function RelacaoProjetoLei() {
    this.id = undefined;
    this.idProjeto = undefined;
    this.idPolitico = undefined;
    this.voto = undefined;
    this.ativo = undefined;
    this.assembreia = undefined;

    // this.perfilSolicitante = new PerfilSolicitante();
    // this.perfilPrestadorServico = new PerfilPrestadorServico();

    this.jsonString = function (senha) {
        var s = this.senha;
        if (!senha) {
            this.setSenhaSemVerificar(undefined);
        }

        var j = JSON.stringify(this);

        if (!senha) {
            this.setSenhaSemVerificar(s);
        }

        return j;
    }

    this.json = function (j) {
        var ok = false;
        if (typeof j == typeof "x") {
            j = JSON.parse(j);
            ok = true;
        } else if (typeof j == typeof new Object) {
            ok = true;
        } else {
            ok = false;
        }
        if (ok && j) {
            // aqui dentro tem que ter todos os sets
            if (j.id == undefined || j.id == "undefined" || j.id == "" || j.id == 0 || j.id == "0") {
                this.setId(j._id); // id do mongo
            } else {
                this.setId(j.id);
            }

            this.setIdProjeto(j.idProjeto);
            this.setIdPolitico(j.idPolitico);
            this.setVoto(j.voto);
            this.setAssembleia(j.assembreia);
            if(j.ativo){
                this.setAtivo(j.ativo);
            }

            // this.setPerfilSolicitante(j.perfilSolicitante);
            // this.setPerfilPrestadorServico(j.perfilPrestadorServico);

            return true;
        } else {
            return false;
        }
    }

    this.setId = function (id) {
        // verificar se eh um id valido (o id do mongo)
        this.id = id;
        return true;
    }

    this.getId = function () {
        return this.id;
    }

    this.setIdProjeto = function (idProjeto){
        this.idProjeto = idProjeto;
        return true;
    }

    this.getIdProjeto = function (){
        return this.idProjeto;
    }

    this.setIdPolitico = function (idPolitico){
        this.idPolitico = idPolitico;
        return true;
    }

    this.getIdPolitico = function (){
        return this.idPolitico;
    }

    this.setVoto = function (voto){
        this.voto = voto;
        return true;
    }

    this.getVoto = function (){
        return this.voto;
    }

    this.setAtivo = function (ativo){
        this.ativo = ativo;
        return true;
    }

    this.getAtivo = function (){
        return this.ativo;
    }

    this.setAssembleia = function (assembreia){
        this.assembreia = assembreia;
        return true;
    }

    this.getAssembleia = function (){
        return this.assembreia;
    }
}

module.exports = RelacaoProjetoLei;