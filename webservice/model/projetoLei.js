#!/bin/env node

function ProjetoLei() {
    this.id = undefined;
    this.descricao = undefined;
    this.tipo = undefined;
    this.data = undefined;
    this.ementa = undefined;
    this.estado = undefined;
    this.idProjeto = undefined;
    this.link = undefined;
    this.nome = undefined;
    this.ativo = undefined;

    // this.perfilSolicitante = new PerfilSolicitante();
    // this.perfilPrestadorServico = new PerfilPrestadorServico();

    this.jsonString = function (senha) {
        var s = this.senha;
        if (!senha) {
            this.setSenhaSemVerificar(undefined);
        }

        var j = JSON.stringify(this);

        if (!senha) {
            this.setSenhaSemVerificar(s);
        }

        return j;
    }

    this.json = function (j) {
        var ok = false;
        if (typeof j == typeof "x") {
            j = JSON.parse(j);
            ok = true;
        } else if (typeof j == typeof new Object) {
            ok = true;
        } else {
            ok = false;
        }
        if (ok && j) {
            // aqui dentro tem que ter todos os sets
            if (j.id == undefined || j.id == "undefined" || j.id == "" || j.id == 0 || j.id == "0") {
                this.setId(j._id); // id do mongo
            } else {
                this.setId(j.id);
            }

            this.setDescricao(j.descricao);
            this.setTipo(j.tipo);
            this.setData(j.data);
            this.setEmenta(j.ementa);
            this.setEstado(j.estado);
            this.setIdProjeto(j.idProjeto);
            this.setLink(j.link);
            this.setNome(j.nome);
            if(j.ativo){
                this.setAtivo(j.ativo);
            }

            // this.setPerfilSolicitante(j.perfilSolicitante);
            // this.setPerfilPrestadorServico(j.perfilPrestadorServico);

            return true;
        } else {
            return false;
        }
    }

    this.setId = function (id) {
        // verificar se eh um id valido (o id do mongo)
        this.id = id;
        return true;
    }

    this.getId = function () {
        return this.id;
    }

    this.setAtivo = function (ativo){
        this.ativo = ativo;
        return true;
    }

    this.getAtivo = function (){
        return this.ativo;
    }

    this.setTipo = function (tipo){
        this.tipo = tipo;
        return true;
    }

    this.getTipo = function (){
        return this.tipo;
    }

    this.setDescricao = function (descricao){
        this.descricao = descricao;
        return true;
    }

    this.getDescricao = function (){
        return this.descricao;
    }

    this.setData = function (data){
        this.data = data;
        return true;
    }

    this.getData = function (){
        return this.data;
    }

    this.setEmenta = function (ementa){
        this.ementa = ementa;
        return true;
    }

    this.getEmenta = function (){
        return this.ementa;
    }

    this.setEstado = function (estado){
        this.estado = estado;
        return true;
    }

    this.getEstado = function (){
        return this.estado;
    }

    this.setIdProjeto = function (idProjeto){
        this.idProjeto = idProjeto;
        return true;
    }

    this.getIdProjeto = function (){
        return this.idProjeto;
    }

    this.setLink = function (link){
        this.link = link;
        return true;
    }

    this.getLink = function (){
        return this.link;
    }

    this.setNome = function (nome){
        this.nome = nome;
        return true;
    }

    this.getNome = function (){
        return this.nome;
    }
}

module.exports = ProjetoLei;