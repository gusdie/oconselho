#!/bin/env node

module.exports = function () {
    // Banco de dados
    require('./conexao.js')();

    // Instanciação dos models
    Usuario = require("../../model/Usuario.js");
    Politico = require("../../model/politico.js");
    Bens = require("../../model/bens.js");
    Partido = require("../../model/partido.js");
    ProjetoLei = require("../../model/projetoLei.js");
    Proposta = require("../../model/proposta.js");
    RelacaoProjetoLei = require("../../model/relacaoProjetoLei.js");
    Seguindo = require("../../model/seguindo.js");
    Social = require("../../model/social.js");
    // Obra = require("../model/Obra.js");
    // PerfilPrestadorServico = require("../model/PerfilPrestadorServico.js");
    // PerfilSolicitante = require("../model/PerfilSolicitante.js");
    // RelacaoObra = require("../model/RelacaoObra.js");

    // Ativação ou instanciação das bibliotecas
    // mhash = require("mhash");
    // uuid = require('node-uuid');
    // //    uuid = require('node-uuid');"node-uuid": "*",uuid.v1()
    // valida = require("../library/Valida.js");
    // Email = require("../library/Email.js");
    // require("../library/Token.js");
    // WebSocketServer = require('ws').Server;

    // Instanciação dos DAOs
    DaoUsuario = require("../../dao/usuario.js");
    DaoPolitico = require("../../dao/politico.js");
    DaoBens = require("../../dao/bens.js");
    DaoPartido = require("../../dao/partido.js");
    DaoProjetoLei = require("../../dao/projetoLei.js");
    DaoProposta = require("../../dao/proposta.js");
    DaoRelacaoProjetoLei = require("../../dao/relacaoProjetoLei.js");
    DaoSeguindo = require("../../dao/seguindo.js");
    DaoSocial = require("../../dao/social.js");
    // DaoObra = require("../db/dao/obra.js");
    // DaoRelacaoObra = require("../db/dao/relacaoObra.js");
};