#!/bin/env node

module.exports = function () {
    var express = require("express");
    var inBot = require('inBot');
    var bodyParser = require('body-parser');

    var app = express();


    app.use(function (req, res, next) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Content-Type", "application/json; charset=utf-8");
    });

    app.use(express.static('./webapp'));
    //app.use(express.static(process.env.OPENSHIFT_DATA_DIR));

    app.use(bodyParser.json())

    app.use(bodyParser.urlencoded({
        extended: true
    }));


    inBot.configura(app);

    // Rotas do app (express)
    //require('../routes/api/obra.js')(app);
    //require('../routes/default.js')(app);
    require('../routes/api/usuario.js')(app);
    require('../routes/api/politico.js')(app);
    require('../routes/api/bens.js')(app);
    require('../routes/api/partido.js')(app);
    require('../routes/api/projetoLei.js')(app);
    require('../routes/api/proposta.js')(app);
    require('../routes/api/relacaoProjetoLei.js')(app);
    require('../routes/api/seguindo.js')(app);
    require('../routes/api/social.js')(app);
    require('../routes/api/popularidade.js')(app);
    //require('../routes/api/relacaoObra.js')(app);
    //require('../routes/api/tags.js')(app);
    require('../routes/api/insercao.js')(app);

    //Bot
    require('../routes/bot/bot.js')(app);



    return app;
};