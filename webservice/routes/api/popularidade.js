#!/bin/env node

var request = require('request');

module.exports = function (app) {
    app.get('/api/popularidade/:idBusca', function (req, res) {
        var idBusca = req.params.idBusca;
        request({
            url: 'http://lb-hackathon-for-dummies-1974423320.sa-east-1.elb.amazonaws.com/mentions/' + idBusca + '?public=Lpd3TkUM &private=HrcmLi3P ',
            method: 'GET',
        }, function (error, response, body) {
            if (error) {
                console.log('Erro ao chamar a API Scup: ', error);
            } else if (response) {

                var votosPositivos = 0;
                var votosNegativos = 0;
                var votosTotais = 0;

                var percentPositivo = 0;
                var percentNegativo = 0;

                var comentarios = JSON.parse(response.body);
                var comentarios = comentarios.data;

                if (!comentarios[0].error_code) {
                    for (var i in comentarios) {
                        var sentiment = comentarios[i].mention.sentiment;
                        if (sentiment === 'positive') {
                            votosPositivos += 1;
                        } else if (sentiment === 'negative') {
                            votosNegativos += 1;
                        }

                    }

                    votosTotais = votosPositivos + votosNegativos;
                    percentPositivo = (votosPositivos / votosTotais) * 100;
                    percentNegativo = (votosNegativos / votosTotais) * 100;

                    var percent = {
                        'positivo': percentPositivo,
                        'negativo': percentNegativo
                    }
                } else {
                    var percent = {
                        'positivo': 0,
                        'negativo': 0
                    }
                }

                res.status(200).json(percent);
            }
        });
    });
}