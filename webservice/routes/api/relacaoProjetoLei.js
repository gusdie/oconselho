#!/bin/env node

module.exports = function (app) {
    app.post('/api/relacaoProjetoLei/cadastrar', function (req, res) {
        var dao = new DaoRelacaoProjetoLei();
        var objeto = new RelacaoProjetoLei();
        if (objeto.json(req.body.relacaoProjetoLei)) {
            dao.add(objeto, function (result) {
                if (result.result) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: result
                    });
                }
            });
        } else {
            res.status(200).json({
                err: 'Erro no formato da relacao projeto lei.'
            });
        }

    });

    app.post('/api/relacaoProjetoLei/recuperar', function (req, res) {
        var dao = new DaoRelacaoProjetoLei();
        var id = req.body.id;
        if(id){
            dao.recuperar(id, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do idRelacaoProjetoLei.'
            });
        }

    });

    app.post('/api/relacaoProjetoLei/recuperarVarias', function (req, res) {
        var dao = new DaoRelacaoProjetoLei();
        var id = req.body.idTse;
        if(id){
            dao.recuperarVarias(id, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do idRelacaoProjetoLei.'
            });
        }

    });

    app.post('/api/relacaoProjetoLei/recuperarLista', function (req, res) {
        var dao = new DaoRelacaoProjetoLei();
        var listaIds = req.body.listaIds;
        if(listaIds && listaIds instanceof Array){

            var lista = new Array();

            for(var i = 0;i < listaIds.length;i++){
                lista.push(ObjectId(listaIds[i]));
            }

            dao.recuperarLista(lista, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do listaIds.'
            });
        }

    });
}