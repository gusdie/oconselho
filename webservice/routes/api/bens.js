#!/bin/env node

module.exports = function (app) {
    app.post('/api/bens/cadastrar', function (req, res) {
        var dao = new DaoBens();
        var objeto = new DaoBens();
        if (objeto.json(req.body.bens)) {
            dao.add(objeto, function (result) {
                if (result.result) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: result
                    });
                }
            });
        } else {
            res.status(200).json({
                err: 'Erro no formato do bens.'
            });
        }

    });

    app.post('/api/bens/recuperar', function (req, res) {
        var dao = new DaoBens();
        var id = req.body.id;
        if(id){
            dao.recuperar(id, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do idBens.'
            });
        }

    });

    app.post('/api/bens/recuperarLista', function (req, res) {
        var dao = new DaoBens();
        var listaIds = req.body.listaIds;
        if(listaIds && listaIds instanceof Array){

            var lista = new Array();

            for(var i = 0;i < listaIds.length;i++){
                lista.push(ObjectId(listaIds[i]));
            }

            dao.recuperarLista(lista, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do listaIds.'
            });
        }

    });

    app.post('/api/bens/recuperarTotalDePoliticoComId', function (req, res) {
        var dao = new DaoBens();
        var id = req.body.idTse;
        if(id){
            dao.total(id, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do idPolitico.'
            });
        }

    });
}