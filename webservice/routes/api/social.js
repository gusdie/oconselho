#!/bin/env node

module.exports = function (app) {
    app.post('/api/social/cadastrar', function (req, res) {
        var dao = new DaoSocial();
        var objeto = new oSocial();
        if (objeto.json(req.body.social)) {
            dao.add(objeto, function (result) {
                if (result.result) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: result
                    });
                }
            });
        } else {
            res.status(200).json({
                err: 'Erro no formato do político.'
            });
        }

    });

    app.post('/api/social/recuperar', function (req, res) {
        var dao = new DaooSocial();
        var id = req.body.id;
        if(id){
            dao.recuperar(id, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do isSocial.'
            });
        }

    });

    app.post('/api/social/recuperarLista', function (req, res) {
        var dao = new DaooSocial();
        var listaIds = req.body.listaIds;
        if(listaIds && listaIds instanceof Array){

            var lista = new Array();

            for(var i = 0;i < listaIds.length;i++){
                lista.push(ObjectId(listaIds[i]));
            }

            dao.recuperarLista(lista, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do listaIds.'
            });
        }

    });
}