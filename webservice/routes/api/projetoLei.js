#!/bin/env node

module.exports = function (app) {
    app.post('/api/projetoLei/cadastrar', function (req, res) {
        var dao = new DaoProjetoLei();
        var objeto = new ProjetoLei();
        if (objeto.json(req.body.projetoLei)) {
            dao.add(objeto, function (result) {
                if (result.result) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: result
                    });
                }
            });
        } else {
            res.status(200).json({
                err: 'Erro no formato do projeto lei.'
            });
        }

    });

    app.post('/api/projetoLei/recuperar', function (req, res) {
        var dao = new DaoProjetoLei();
        var id = req.body.id;
        if(id){
            dao.recuperar(id, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do idProjetoLei.'
            });
        }

    });

    app.post('/api/projetoLei/recuperarLista', function (req, res) {
        var dao = new DaoProjetoLei();
        var listaIds = req.body.listaIds;
        if(listaIds && listaIds instanceof Array){

            var lista = new Array();

            for(var i = 0;i < listaIds.length;i++){
                lista.push(ObjectId(listaIds[i]));
            }

            dao.recuperarLista(lista, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do listaIds.'
            });
        }

    });
}