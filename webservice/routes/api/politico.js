#!/bin/env node

var async = require('async');

module.exports = function (app) {
    app.post('/api/politico/cadastrar', function (req, res) {
        var dao = new DaoPolitico();
        var objeto = new Politico();
        if (objeto.json(req.body.politico)) {
            dao.add(objeto, function (result) {
                if (result.result) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: result
                    });
                }
            });
        } else {
            res.status(200).json({
                err: 'Erro no formato do político.'
            });
        }

    });

    app.post('/api/politico/recuperar', function (req, res) {
        var dao = new DaoPolitico();
        var id = req.body.id;
        if (id) {
            dao.recuperar(id, function (err, result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        } else {
            res.status(200).json({
                err: 'Erro no formato do idPolitico.'
            });
        }

    });

    app.post('/api/politico/recuperarLista', function (req, res) {
        var dao = new DaoPolitico();
        var listaIds = req.body.listaIds;
        if (listaIds && listaIds instanceof Array) {

            var lista = new Array();

            for (var i = 0; i < listaIds.length; i++) {
                lista.push(ObjectId(listaIds[i]));
            }

            dao.recuperarLista(lista, function (err, result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        } else {
            res.status(200).json({
                err: 'Erro no formato do listaIds.'
            });
        }

    });

    app.post('/api/politico/consultaPrefeitos', function (req, res) {
        var dao = new DaoPolitico();
        dao.recuperarCargos("PREFEITO", 11, function (err, result) {
            if (err) {
                res.status(200).json({
                    err: err,
                    result: null
                });
            }
            res.status(200).json({
                err: null,
                result: result
            });
        });

    });

    app.post('/api/politico/consultaVereadores', function (req, res) {
        var dao = new DaoPolitico();
        dao.recuperarCargos("VEREADOR", 20, function (err, result) {
            if (err) {
                res.status(200).json({
                    err: err,
                    result: null
                });
            }
            res.status(200).json({
                err: null,
                result: result
            });
        });

    });

//    app.post('/api/politico/consultaGeralPrefeito', function (req, res) {
            //
            //        var total = new Array();
            //        var index = 0;
            //
            //        var dao = new DaoPolitico();
            //
            //        dao.recuperarCargos("PREFEITO", 11, function (err, result) {
            //            if (err) {
            //                res.status(200).json({
            //                    err: err,
            //                    result: null
            //                });
            //            }
            //
            //            listaPrefeitos = [];
            //
            //            for (var i = 0; i < 1; i++) {
            //
            //                var daoBens = new DaoBens();
            //                var daoProposta = new DaoProposta();
            //                var daoRelacaoProjetoLei = new DaoRelacaoProjetoLei();
            //                prefeito = new Politico();
            //                if (!prefeito.json(result[i])) {
            //                    res.status(200).json({
            //                        err: 'Erro ao passar json para politico',
            //                        result: null
            //                    });
            //                }
            //                console.log('PREFEITO');
            //                console.log(prefeito);
            //
            //                retorno1 = 0;
            //                retorno2 = 0;
            //
            //
            //                while (retorno1 != 1 && retorno2 != 1) {
            //                    console.log('ID PREFEITO ', prefeito.getId());
            //                    daoBens.total(prefeito.getId(), function (err2, result2) {
            //                        if (err2) {
            //                            console.log('ERRO 1 ', err2);
            //                            res.status(200).json({
            //                                err: err2,
            //                                result: null
            //                            });
            //                        } else {
            //                            console.log('TOTAL BENS ', result2);
            //                            retorno1 = 1;
            //                        }
            //                    });
            //
            //                    daoProposta.gerais(prefeito.getId(), 5, function (err3, result3) {
            //                        if (err3) {
            //                            console.log('ERRO 2 ', err3);
            //                            res.status(200).json({
            //                                err: err3,
            //                                result: null
            //                            });
            //                        } else {
            //                            prefeito.propostasGerais = result3;
            //                            console.log('PROPOSTAS GERAIS ', result3);
            //                            retorno2 = 1;
            //                        }
            //                    });
            //                }
            //
            //                if (retorno1 == 1 && retorno2 == 1) {
            //                    listaPrefeitos.push(prefeito);
            //                    console.log(listaPrefeitos);
            //                }
            //
            //                if (i == 11) {
            //                    console.log('FINALIZADO');
            //                }
            //
            //                var retorno1 = 0;
            //                var retorno2 = 0;
            //
            //            }
            //        });
            //    });

    app.post('/api/politico/consultaGeralVereador', function (req, res) {
        var total = new Array();
        var index = 0;

        var dao = new DaoPolitico();

        dao.recuperarCargos("VEREADOR", 20, function (err, result) {
            if (err) {
                res.status(200).json({
                    err: err,
                    result: null
                });
            }
            for (p in result) {
                var daoBens = new DaoBens();
                var vereador = new Politico();
                if (!vereador.json(p)) {
                    res.status(200).json({
                        err: 'Erro ao passar json para politico',
                        result: null
                    });
                }
                daoBens.total(vereador.getId(), function (err2, result2) {
                    if (err2) {
                        res.status(200).json({
                            err: err2,
                            result: null
                        });
                    }
                    vereador.totalBens = result2;
                    var daoRelacaoProjetoLei = new DaoRelacaoProjetoLei();
                    var array = new Array();
                    array.push(vereador.getId());
                    daoRelacaoProjetoLei.recuperarLista(array, function (err4, result4) {
                        if (err4) {
                            res.status(200).json({
                                err: err4,
                                result: null
                            });
                            p.relacoes = result4;
                            total.push(vereador);
                            if (index == result.length) {
                                res.status(200).json({
                                    err: null,
                                    result: total
                                });
                            }
                            index = index + 1;
                        }
                    });
                });
            }
        });
    });
}