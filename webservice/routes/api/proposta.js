#!/bin/env node

module.exports = function (app) {
    app.post('/api/proposta/cadastrar', function (req, res) {
        var dao = new DaoProposta();
        var objeto = new Proposta();
        if (objeto.json(req.body.proposta)) {
            dao.add(objeto, function (result) {
                if (result) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: result
                    });
                }
            });
        } else {
            res.status(200).json({
                err: 'Erro no formato do político.'
            });
        }

    });

    app.post('/api/proposta/recuperar', function (req, res) {
        var dao = new DaoProposta();
        var id = req.body.id;
        if(id){
            dao.recuperar(id, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do idProposta.'
            });
        }

    });

    app.post('/api/proposta/geralPolitico', function (req, res) {
        var dao = new DaoProposta();
        var id = req.body.idTse;
        if(id){
            dao.gerais(id,5, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do idTse da proposta.'
            });
        }

    });

    app.post('/api/relacaoProjetoLei/recuperarVarias', function (req, res) {
        var dao = new DaoProposta();
        var id = req.body.idTse;
        if(id){
            dao.recuperarVarias(id, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do idRelacaoProjetoLei.'
            });
        }

    });


    app.post('/api/proposta/recuperarLista', function (req, res) {
        var dao = new DaoProposta();
        var listaIds = req.body.listaIds;
        if(listaIds && listaIds instanceof Array){

            var lista = new Array();

            for(var i = 0;i < listaIds.length;i++){
                lista.push(ObjectId(listaIds[i]));
            }

            dao.recuperarLista(lista, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do listaIds.'
            });
        }

    });
}