#!/bin/env node

module.exports = function (app) {
    app.post('/api/usuario/cadastrar', function (req, res) {
        var daoUsuario = new DaoUsuario();
        var usuario = new Usuario();
        if (usuario.json(req.body.usuario)) {
            daoUsuario.add(usuario, function (result) {
                if (result.result) {
                    res.status(200).json({
                        result: result.ops[0]._id
                    });
                } else {
                    res.status(200).json({
                        err: result.err
                    });
                }
            });
        } else {
            res.status(200).json({
                err: 'Erro no formato do usuário.'
            });
        }

    });

    app.post('/api/usuario/recuperar', function (req, res) {
        var daoUsuario = new DaoUsuario();
        var idUsuario = req.body.idUsuario;
        if(idUsuario){
            daoUsuario.recuperar(idUsuario, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do idUsuario.'
            });
        }

    });

    app.post('/api/usuario/atualizarMessagerId', function (req, res) {
        var daoUsuario = new DaoUsuario();
        var idUsuario = req.body.idUsuario;
        var mid = req.body.mid;
        if(idUsuario){
            daoUsuario.atualizarMessagerIdUsuarioComId(mid,idUsuario, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do idUsuario.'
            });
        }

    });

    app.post('/api/usuario/recuperarLista', function (req, res) {
        var daoUsuario = new DaoUsuario();
        var listaIds = req.body.listaIds;
        console.log(listaIds);
        if(listaIds && listaIds instanceof Array){

            var lista = new Array();

            for(var i = 0;i < listaIds.length;i++){
                lista.push(ObjectId(listaIds[i]));
            }

            daoUsuario.recuperarLista(lista, function (err,result) {
                if (!err) {
                    res.status(200).json({
                        result: result
                    });
                } else {
                    res.status(200).json({
                        err: err
                    });
                }
            });
        }
        else{
            res.status(200).json({
                err: 'Erro no formato do listaIds.'
            });
        }

    });
}