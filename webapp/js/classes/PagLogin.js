function PagLogin() {

    this.construtor = function () {
        this.montaPagHtml();
    }

    this.montaPagHtml = function () {
        pagina.getMain().empty();
        var row = document.createElement("div");
        row.className = "row";
        row.style.marginTop = "7em";

        var col = document.createElement("div");
        col.className = "col s12 center";
        col.style.marginBottom = "2em";

        var img = document.createElement("img");
        img.src = "img/logo.png";
        img.style.width = "5em";
        img.style.height = "5em";
        col.appendChild(img);

        row.appendChild(col);


        var col = document.createElement("div");
        col.className = "col s12 center";
        var span = document.createElement("p");
        span.style.color = "#C3C75D";
        span.style.fontSize = "1.5em";
        span.appendChild(document.createTextNode("OCONSELHO"));
        col.appendChild(span);
        var span = document.createElement("span");
        span.style.color = "#BABABA";
        span.appendChild(document.createTextNode("Todos os candidatos no seu radar."));
        col.appendChild(span);
        row.appendChild(col);

        var col = document.createElement("div");
        col.className = "col s12";
        col.style.marginTop = "4em";
        var a_0 = document.createElement('a');
        a_0.className = "light-blue darken-4 waves-effect waves-light btn-large btn-xl";
        a_0.style.fontSize = "0.9em";
        var i = document.createElement("i");
        i.className = "mdi mdi-facebook left";
        a_0.appendChild(i);
        a_0.appendChild(document.createTextNode("Acesse com o facebook"));
        a_0.addEventListener("click", function () {
            checkLoginState();
        });
        col.appendChild(a_0);
        row.appendChild(col);
        var col = document.createElement("div");
        col.className = "col s12 center";
        var a_0 = document.createElement('a');
        a_0.className = "waves-effect waves-light btn-flat";
        a_0.setAttribute("style", "color:#9D9D9D;font-size:0.9em;");
        a_0.appendChild(document.createTextNode("Entrar sem logar"));
        a_0.addEventListener("click", function () {
            pagina.abrirPaginas("Bens");

        });
        col.appendChild(a_0);
        row.appendChild(col);
        pagina.getMain().appendChild(row);
    }
    getTitle("Oconselho - Login");
    this.construtor();
}