function Pagina() {
    this.getMain = function () {
        return document.getElementsByTagName("main")[0];
    }

    this.apiUrl = function () {
        return "https://oconselho-aoub.rhcloud.com/api/";
    }

    this.abrirPaginas = function (novaPagina) {
        document.getElementsByTagName("header")[0].style.display = "block";
        document.getElementById("liBens").style.display = "block";
        document.getElementById("liProjetos").style.display = "block";
        document.getElementById("liPropostas").style.display = "block";
        document.getElementById("liDados").style.display = "block";
        document.getElementById("liPopularidade").style.display = "block";
        if (novaPagina == "Bens") {
            document.getElementById("btnDropdown").innerHTML = "Bens <i class='mdi mdi-menu-down'  style='display:inherit !important'></i>";
            document.getElementById("liBens").style.display = "none";
            pag = new PagBens();

        } else if (novaPagina == "Projetos") {
            document.getElementById("btnDropdown").innerHTML = "Projetos <i class='mdi mdi-menu-down'  style='display:inherit !important'></i>";
            document.getElementById("liProjetos").style.display = "none";
            pag = new PagProjetos();

        } else if (novaPagina == "Propostas") {
            document.getElementById("btnDropdown").innerHTML = "Propostas <i class='mdi mdi-menu-down'  style='display:inherit !important'></i>";
            document.getElementById("liPropostas").style.display = "none";
            pag = new PagPropostas();

        } else if (novaPagina == "Dados") {
            document.getElementById("btnDropdown").innerHTML = "Dados <i class='mdi mdi-menu-down'  style='display:inherit !important'></i>";
            document.getElementById("liDados").style.display = "none";
            pag = new PagDados();
        } else if (novaPagina == "Popularidade") {
            document.getElementById("btnDropdown").innerHTML = "Popularidade <i class='mdi mdi-menu-down'  style='display:inherit !important'></i>";
            document.getElementById("liPopularidade").style.display = "none";
            pag = new PagPopularidade();
        } else if (novaPagina == "Login") {
            document.getElementsByTagName("header")[0].style.display = "none";
            console.log("teste");
            pag = new PagLogin();
        } else {
            console.log("PAGINA NAO ESPERADA: " + novaPagina);
            return false;
        }
        return true;
    }
}
var title = undefined;

function getTitle(title) {
    document.getElementsByTagName("title")[0].innerHTML = title;
}