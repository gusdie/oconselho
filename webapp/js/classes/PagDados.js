function PagDados() {

    this.construtor = function () {
        this.montaPagHtml();
    }

    this.montaPagHtml = function () {
        pagina.getMain().empty();


        var nomePoliticos = ["João Cintra", "Laura Peixoto", "Luís Gustavo"];
        var urlPoliticos = ["img/img1.jpg", "img/img2.jpg", "img/img3.jpg"];
        var partidoPoliticos = ["pcb", "pt", "pco"];
        var bens = [20000, 90000, 160000];
        var div_0 = document.createElement('div');
        div_0.style.marginTop = "4.5em";
        div_0.className = "row";

        var div_1 = document.createElement('div');
        div_1.className = "col s12";

        var ul_0 = document.createElement('ul');
        ul_0.className = "tabs";

        var li_0 = document.createElement('li');
        li_0.className = "tab col s3 montserrat-bold";

        var a_0 = document.createElement('a');
        a_0.href = "#prefeitos";
        a_0.className = "active";
        a_0.appendChild(document.createTextNode("Prefeitos"));
        li_0.appendChild(a_0);

        ul_0.appendChild(li_0);


        var li_1 = document.createElement('li');
        li_1.className = "tab col s3 montserrat-bold";

        var a_1 = document.createElement('a');
        a_1.href = "#vereadores";
        a_1.appendChild(document.createTextNode("Vereadores"));
        li_1.appendChild(a_1);

        ul_0.appendChild(li_1);

        div_1.appendChild(ul_0);

        div_0.appendChild(div_1);


        var div_prefeitos = document.createElement('div');
        div_prefeitos.id = "prefeitos";
        div_prefeitos.className = "col s12";
        div_prefeitos.style.marginTop = "1em";



        div_0.appendChild(div_prefeitos);


        var div_vereadores = document.createElement('div');
        div_vereadores.id = "vereadores";
        div_vereadores.className = "col s12";
        div_vereadores.style.marginTop = "1em";

        div_0.appendChild(div_vereadores);

        pagina.getMain().appendChild(div_0);

        this.montaCarrossel(div_prefeitos);

        this.montaDados(pagina.getMain());


        $('ul.tabs').tabs();


        var elem = document.querySelector('.carousel');
        var flkty = new Flickity(elem);
        politico = flkty.selectedElement;
        document.getElementById('nomePolitico').innerHTML = "<span class='montserrat-bold'>" + politico.getAttribute("nomePolitico") + "</span>";
        document.getElementById('partidoPolitico').innerHTML = "<span class='montserrat'>" + politico.getAttribute("partidoPolitico") + "</span>";


        return true;

    }
    this.montaCarrossel = function (pai) {
        var nomePoliticos = ["João Cintra", "Laura Peixoto", "Luís Gustavo"];
        var urlPoliticos = ["img/img1.jpg", "img/img2.jpg", "img/img3.jpg"];
        var partidoPoliticos = ["pcb", "pt", "pco"];
        var bens = [20000, 90000, 160000];
        var div_2 = document.createElement('div');
        div_2.className = "carousel";

        for (i = 0; i < nomePoliticos.length; i++) {
            var div_3 = document.createElement('div');
            div_3.className = "carousel-cell";
            div_3.setAttribute("partidopolitico", "20 PSOL");
            div_3.setAttribute("value", bens[i]);
            div_3.setAttribute("nomepolitico", nomePoliticos[i]);
            div_3.style.backgroundImage = "url(" + urlPoliticos[i] + ")";

            var div_4 = document.createElement('div');
            div_4.className = "right";
            div_4.style.display = "flex";
            div_4.style.flexDirection = "column";

            var img_0 = document.createElement('img');
            img_0.style.width = "2.5em";
            img_0.src = "img/partidos/" + partidoPoliticos[i] + ".png";
            img_0.className = "right";
            div_4.appendChild(img_0);


            var div_5 = document.createElement('div');
            div_5.style.height = "8.5em";
            div_4.appendChild(div_5);


            var div_6 = document.createElement('div');
            div_6.style.width = "3em";
            div_6.style.height = "3em";
            div_6.style.borderRadius = "30em";
            div_6.style.display = "flex";
            div_6.style.justifyContent = "center";
            div_6.style.alignItems = "center";
            div_6.style.top = "14em";
            div_6.className = "black white-text";

            var i_0 = document.createElement('i');
            i_0.className = "mdi mdi-radar ";
            i_0.style.fontSize = "2em";
            div_6.appendChild(i_0);

            div_4.appendChild(div_6);

            div_3.appendChild(div_4);

            div_2.appendChild(div_3);
        }
        pai.appendChild(div_2);

    }


    this.montaDados = function (pai, politico) {
        var dados = ["APELIDO", "PARTIDO", "CANDIDATO", "INSTRUÇÃO", "OCUPAÇÃO"];
        var dadosInfo = ["João Cintra", "pt", "13", "SUPERIOR COMPLETO", "Casado"];
        var div_0 = document.createElement('div');
        div_0.className = "container";

        var div_nomePolitico = document.createElement('div');
        div_nomePolitico.id = "nomePolitico";
        div_nomePolitico.className = "center";

        var span_0 = document.createElement('span');
        span_0.className = "montserrat-bold";
        div_nomePolitico.appendChild(span_0);

        div_0.appendChild(div_nomePolitico);


        var div_partidoPolitico = document.createElement('div');
        div_partidoPolitico.className = "center";
        div_partidoPolitico.id = "partidoPolitico";

        var span_1 = document.createElement('span');
        span_1.className = "montserrat";
        div_partidoPolitico.appendChild(span_1);

        div_0.appendChild(div_partidoPolitico);


        var div_1 = document.createElement('div');
        div_1.className = "center";

        var h5_0 = document.createElement('h5');
        h5_0.className = "montserrat-bold";
        h5_0.appendChild(document.createTextNode("Dados Básicos:"));
        div_1.appendChild(h5_0);

        div_0.appendChild(div_1);

        pai.appendChild(div_0);


        for (j = 0; j < dados.length; j++) {

            var div_0 = document.createElement('div');
            div_0.style.padding = "1em";
            div_0.style.backgroundColor = "#c3c664";
            div_0.style.borderBottom = "1px solid #cccccc";

            var div_1 = document.createElement('div');
            div_1.className = "white-text";
            div_1.appendChild(document.createTextNode(dados[j]));
            div_0.appendChild(div_1);


            var div_2 = document.createElement('div');
            div_2.style.color = "rgba(69, 69, 69, 0.6)";
            div_2.appendChild(document.createTextNode(dadosInfo[j]));
            div_0.appendChild(div_2);

            pai.appendChild(div_0);

        }


    }
    getTitle("Oconselho - Dados Básicos");

    this.construtor();
}