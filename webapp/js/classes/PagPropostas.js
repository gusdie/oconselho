function PagPropostas() {

    this.construtor = function () {
        this.montaPagHtml();
    }

    this.montaPagHtml = function () {
        pagina.getMain().empty();


        var nomePoliticos = ["Marcos", "Maria", "Jorge"];
        var urlPoliticos = ["img/img1.jpg", "img/img2.jpg", "img/img3.jpg"];
        var partidoPoliticos = ["pcb", "pt", "pco"];
        var bens = [20000, 90000, 160000];
        var div_0 = document.createElement('div');
        div_0.style.marginTop = "4.5em";
        div_0.className = "row";

        var div_1 = document.createElement('div');
        div_1.className = "col s12";

        var ul_0 = document.createElement('ul');
        ul_0.className = "tabs";

        var li_0 = document.createElement('li');
        li_0.className = "tab col s3 montserrat-bold";

        var a_0 = document.createElement('a');
        a_0.href = "#prefeitos";
        a_0.className = "active";
        a_0.appendChild(document.createTextNode("Prefeitos"));
        li_0.appendChild(a_0);

        ul_0.appendChild(li_0);

        div_1.appendChild(ul_0);

        div_0.appendChild(div_1);


        var div_prefeitos = document.createElement('div');
        div_prefeitos.id = "prefeitos";
        div_prefeitos.className = "col s12";
        div_prefeitos.style.marginTop = "1em";



        div_0.appendChild(div_prefeitos);

        pagina.getMain().appendChild(div_0);

        this.montaCarrossel(div_prefeitos);

        this.montaPropostas(pagina.getMain());

        $('ul.tabs').tabs();


        var elem = document.querySelector('.carousel');
        var flkty = new Flickity(elem);
        politico = flkty.selectedElement;
        document.getElementById('nomePolitico').innerHTML = "<span class='montserrat-bold'>" + politico.getAttribute("nomePolitico") + "</span>";
        document.getElementById('partidoPolitico').innerHTML = "<span class='montserrat'>" + politico.getAttribute("partidoPolitico") + "</span>";


        return true;

    }
    this.montaCarrossel = function (pai) {
        var nomePoliticos = ["Marcos", "Maria", "Jorge"];
        var urlPoliticos = ["img/img1.jpg", "img/img2.jpg", "img/img3.jpg"];
        var partidoPoliticos = ["pcb", "pt", "pco"];
        var bens = [20000, 90000, 160000, 20000, 90000, 160000, 20000, 90000, 160000, 160000];
        var div_2 = document.createElement('div');
        div_2.className = "carousel";

        for (i = 0; i < nomePoliticos.length; i++) {
            var div_3 = document.createElement('div');
            div_3.className = "carousel-cell";
            div_3.setAttribute("partidopolitico", "20 PSOL");
            div_3.setAttribute("value", bens[i]);
            div_3.setAttribute("nomepolitico", nomePoliticos[i]);
            div_3.style.backgroundImage = "url(" + urlPoliticos[i] + ")";

            var div_4 = document.createElement('div');
            div_4.className = "right";
            div_4.style.display = "flex";
            div_4.style.flexDirection = "column";

            var span = document.createElement("span");
            span.setAttribute("aria-label", "Partido");
            span.className = "hint--top";
            var img_0 = document.createElement('img');
            img_0.style.width = "2.5em";
            img_0.src = "img/partidos/" + partidoPoliticos[i] + ".png";
            img_0.className = "right";
            span.appendChild(img_0);
            div_4.appendChild(span);


            var div_5 = document.createElement('div');
            div_5.style.height = "11.5em";
            div_4.appendChild(div_5);


            var div_6 = document.createElement('div');
            div_6.style.width = "3em";
            div_6.style.height = "3em";
            div_6.setAttribute("aria-label", "Seguir");
            div_6.style.borderRadius = "30em";
            div_6.style.left = "0.8em";
            div_6.style.display = "flex";
            div_6.style.justifyContent = "center";
            div_6.style.alignItems = "center";
            div_6.className = "white-text hint--top";
            div_6.style.backgroundColor = "#000000";
            div_6.addEventListener("click", function (e) {
                if (this.style.backgroundColor == "rgb(0, 0, 0)") {
                    this.setAttribute("aria-label", "Seguindo");
                    this.style.backgroundColor = "#346A66";
                } else {
                    this.setAttribute("aria-label", "Seguir");
                    this.style.backgroundColor = "rgb(0, 0, 0)";
                }
            });

            var i_0 = document.createElement('i');
            i_0.className = "mdi mdi-radar ";
            i_0.style.fontSize = "2em";
            div_6.appendChild(i_0);

            div_4.appendChild(div_6);

            div_3.appendChild(div_4);

            div_2.appendChild(div_3);
        }
        pai.appendChild(div_2);

    }

    this.montaPropostas = function (pai, politico) {
        var temas = ["EDUCAÇÃO", "MOBILIDADE URBANA", "SAÚDE", "CULTURA", "SEGURANÇA"];
        var temasDesc = ["Aumentar continuamente o investimento em educação de 3 maneiras: 1) articulação política para aumentar a porcentagem dos recursos próprios da cidade que são aplicados em educação; 2) criação de projetos intersetoriais que aplicam recursos em educação de maneira indireta; e 3) associação com projetos que usam fontes alternativas de recurso, como de instituições privadas e da própria comunidade.", "Aumentar continuamente o investimento em educação de 3 maneiras: 1) articulação política para aumentar a porcentagem dos recursos próprios da cidade que são aplicados em educação; 2) criação de projetos intersetoriais que aplicam recursos em educação de maneira indireta; e 3) associação com projetos que usam fontes alternativas de recurso, como de instituições privadas e da própria comunidade.", "Aumentar continuamente o investimento em educação de 3 maneiras: 1) articulação política para aumentar a porcentagem dos recursos próprios da cidade que são aplicados em educação; 2) criação de projetos intersetoriais que aplicam recursos em educação de maneira indireta; e 3) associação com projetos que usam fontes alternativas de recurso, como de instituições privadas e da própria comunidade.", "Aumentar continuamente o investimento em educação de 3 maneiras: 1) articulação política para aumentar a porcentagem dos recursos próprios da cidade que são aplicados em educação; 2) criação de projetos intersetoriais que aplicam recursos em educação de maneira indireta; e 3) associação com projetos que usam fontes alternativas de recurso, como de instituições privadas e da própria comunidade.", "Aumentar continuamente o investimento em educação de 3 maneiras: 1) articulação política para aumentar a porcentagem dos recursos próprios da cidade que são aplicados em educação; 2) criação de projetos intersetoriais que aplicam recursos em educação de maneira indireta; e 3) associação com projetos que usam fontes alternativas de recurso, como de instituições privadas e da própria comunidade."];
        var div_0 = document.createElement('div');
        div_0.className = "container";

        var div_nomePolitico = document.createElement('div');
        div_nomePolitico.id = "nomePolitico";
        div_nomePolitico.className = "center";

        var span_0 = document.createElement('span');
        span_0.className = "montserrat-bold";
        div_nomePolitico.appendChild(span_0);

        div_0.appendChild(div_nomePolitico);


        var div_partidoPolitico = document.createElement('div');
        div_partidoPolitico.className = "center";
        div_partidoPolitico.id = "partidoPolitico";

        var span_1 = document.createElement('span');
        span_1.className = "montserrat";
        div_partidoPolitico.appendChild(span_1);

        div_0.appendChild(div_partidoPolitico);


        var div_1 = document.createElement('div');
        div_1.className = "center";

        var h5_0 = document.createElement('h5');
        h5_0.className = "montserrat-bold";
        h5_0.appendChild(document.createTextNode("Propostas:"));
        div_1.appendChild(h5_0);

        div_0.appendChild(div_1);

        pai.appendChild(div_0);

        var col = document.createElement('div');
        col.className = "col s12";

        var ul = document.createElement('ul');
        ul.className = "collapsible";
        ul.setAttribute("data-collapsible", "accordion");
        for (j = 0; j < temas.length; j++) {
            var li = document.createElement('li');
            var divHeader = document.createElement('div');
            divHeader.className = "collapsible-header pointer";
            divHeader.style.padding = "0.5em";
            divHeader.style.backgroundColor = "#90c664";
            divHeader.style.borderBottom = "1px solid #cccccc";
            var div_3 = document.createElement('div');
            div_3.className = "white-text";
            div_3.appendChild(document.createTextNode(temas[j]));
            divHeader.appendChild(div_3);


            var div_4 = document.createElement('div');
            div_4.style.color = "rgba(69, 69, 69, 0.6)";
            div_4.style.fontSize = "0.7em";
            div_4.appendChild(document.createTextNode("PROPOSTAS DO CANDIDATO"));
            divHeader.appendChild(div_4);

            li.appendChild(divHeader);

            var divBody = document.createElement('div');
            divBody.className = "collapsible-body pointer";

            var div_7 = document.createElement('div');
            div_7.style.padding = "1em";
            div_7.className = "col s12";

            var p_0 = document.createElement('span');
            p_0.appendChild(document.createTextNode(temasDesc[j]));
            div_7.appendChild(p_0);

            divBody.appendChild(div_7);

            var div_8 = document.createElement('div');
            div_8.style.padding = "1em 0em";
            div_8.className = "col s12 grey lighten-3 center";


            var p_1 = document.createElement('span');

            var strong_0 = document.createElement('strong');
            strong_0.appendChild(document.createTextNode("DESCRIÇÃO DA PROPOSTA"));
            p_1.appendChild(strong_0);

            div_8.appendChild(p_1);

            divBody.appendChild(div_8);
            li.appendChild(divBody);
            ul.appendChild(li);

        }
        col.appendChild(ul);
        pai.appendChild(col);
        $('.collapsible').collapsible();

    }
    getTitle("Oconselho - Propostas");

    this.construtor();
}