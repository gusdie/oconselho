function PagProjetos() {

    this.construtor = function () {
        this.montaPagHtml();
    }

    this.montaPagHtml = function () {
        pagina.getMain().empty();


        var nomePoliticos = ["João", "Julia", "Rafael"];
        var urlPoliticos = ["img/img1.jpg", "img/img2.jpg", "img/img3.jpg"];
        var partidoPoliticos = ["pcb", "pt", "pco"];
        var bens = [20000, 90000, 160000];
        var div_0 = document.createElement('div');
        div_0.style.marginTop = "4.5em";
        div_0.className = "row";

        var div_1 = document.createElement('div');
        div_1.className = "col s12";

        var ul_0 = document.createElement('ul');
        ul_0.className = "tabs";


        var li_1 = document.createElement('li');
        li_1.className = "tab col s3 montserrat-bold";

        var a_1 = document.createElement('a');
        a_1.href = "#vereadores";
        a_1.appendChild(document.createTextNode("Vereadores"));
        li_1.appendChild(a_1);

        ul_0.appendChild(li_1);

        div_1.appendChild(ul_0);

        div_0.appendChild(div_1);


        var div_vereadores = document.createElement('div');
        div_vereadores.id = "vereadores";
        div_vereadores.className = "col s12";
        div_vereadores.style.marginTop = "1em";
        this.montaCarrossel(div_vereadores);
        div_0.appendChild(div_vereadores);

        pagina.getMain().appendChild(div_0);



        var div_0 = document.createElement('div');
        div_0.className = "container";

        var div_nomePolitico = document.createElement('div');
        div_nomePolitico.id = "nomePolitico";
        div_nomePolitico.className = "center";

        var span_0 = document.createElement('span');
        span_0.className = "montserrat-bold";
        div_nomePolitico.appendChild(span_0);

        div_0.appendChild(div_nomePolitico);


        var div_partidoPolitico = document.createElement('div');
        div_partidoPolitico.className = "center";
        div_partidoPolitico.id = "partidoPolitico";

        var span_1 = document.createElement('span');
        span_1.className = "montserrat";
        div_partidoPolitico.appendChild(span_1);

        div_0.appendChild(div_partidoPolitico);


        var div_1 = document.createElement('div');
        div_1.className = "center";

        var h5_0 = document.createElement('h5');
        h5_0.className = "montserrat-bold";
        h5_0.appendChild(document.createTextNode("Projetos:"));
        div_1.appendChild(h5_0);

        div_0.appendChild(div_1);

        pagina.getMain().appendChild(div_0);



        var div_0 = document.createElement('div');
        div_0.className = "card";

        var div_1 = document.createElement('div');
        div_1.className = "col s12";

        var ul_0 = document.createElement('ul');
        ul_0.className = "tabs tabs2";

        var li_0 = document.createElement('li');
        li_0.className = "tab tab2 col s3";

        var a_0 = document.createElement('a');
        a_0.href = "#test1";
        a_0.className = "active";
        a_0.appendChild(document.createTextNode("Projetos a favor"));
        li_0.appendChild(a_0);

        ul_0.appendChild(li_0);


        var li_1 = document.createElement('li');
        li_1.className = "tab tab2 col s3";

        var a_1 = document.createElement('a');
        a_1.href = "#test2";
        a_1.appendChild(document.createTextNode("Projetos contra"));
        li_1.appendChild(a_1);

        ul_0.appendChild(li_1);

        div_1.appendChild(ul_0);

        div_0.appendChild(div_1);


        var div_test1 = document.createElement('div');
        div_test1.className = "col s12";
        div_test1.id = "test1";

        this.montaProjetos(div_test1);


        div_0.appendChild(div_test1);


        var div_test2 = document.createElement('div');
        div_test2.className = "col s12";
        div_test2.id = "test2";
        div_test2.appendChild(document.createTextNode("Test 2"));
        div_0.appendChild(div_test2);

        pagina.getMain().appendChild(div_0);
        $('ul.tabs').tabs();
        $('.collapsible').collapsible();

        var elem = document.querySelector('.carousel');
        var flkty = new Flickity(elem);
        politico = flkty.selectedElement;
        document.getElementById('nomePolitico').innerHTML = "<span class='montserrat-bold'>" + politico.getAttribute("nomePolitico") + "</span>";
        document.getElementById('partidoPolitico').innerHTML = "<span class='montserrat'>" + politico.getAttribute("partidoPolitico") + "</span>";

        return true;

    }
    this.montaCarrossel = function (pai) {
        var nomePoliticos = ["Michel", "Guilherme", "Gustavo", "Michel", "Guilherme", "Gustavo", "Michel", "Guilherme", "Gustavo", "Gustavo"];
        var urlPoliticos = ["img/img1.jpg", "img/img2.jpg", "img/img3.jpg", "img/img1.jpg", "img/img2.jpg", "img/img3.jpg", "img/img1.jpg", "img/img2.jpg", "img/img3.jpg", "img/img3.jpg"];
        var partidoPoliticos = ["pcb", "pt", "pco", "pcb", "pt", "pco", "pcb", "pt", "pco", "pco"];
        var bens = [20000, 90000, 160000, 20000, 90000, 160000, 20000, 90000, 160000, 160000];
        var div_2 = document.createElement('div');
        div_2.className = "carousel";

        for (i = 0; i < nomePoliticos.length; i++) {
            var div_3 = document.createElement('div');
            div_3.className = "carousel-cell";
            div_3.setAttribute("partidopolitico", "20 PSOL");
            div_3.setAttribute("value", bens[i]);
            div_3.setAttribute("nomepolitico", nomePoliticos[i]);
            div_3.style.backgroundImage = "url(" + urlPoliticos[i] + ")";

            var div_4 = document.createElement('div');
            div_4.className = "right";
            div_4.style.display = "flex";
            div_4.style.flexDirection = "column";

            var span = document.createElement("span");
            span.setAttribute("aria-label", "Partido");
            span.className = "hint--top";
            var img_0 = document.createElement('img');
            img_0.style.width = "2.5em";
            img_0.src = "img/partidos/" + partidoPoliticos[i] + ".png";
            img_0.className = "right";
            span.appendChild(img_0);
            div_4.appendChild(span);


            var div_5 = document.createElement('div');
            div_5.style.height = "11.5em";
            div_4.appendChild(div_5);


            var div_6 = document.createElement('div');
            div_6.style.width = "3em";
            div_6.style.height = "3em";
            div_6.setAttribute("aria-label", "Seguir");
            div_6.style.borderRadius = "30em";
            div_6.style.left = "0.8em";
            div_6.style.display = "flex";
            div_6.style.justifyContent = "center";
            div_6.style.alignItems = "center";
            div_6.className = "white-text hint--top";
            div_6.style.backgroundColor = "#000000";
            div_6.addEventListener("click", function (e) {
                if (this.style.backgroundColor == "rgb(0, 0, 0)") {
                    this.setAttribute("aria-label", "Seguindo");
                    this.style.backgroundColor = "#346A66";
                } else {
                    this.setAttribute("aria-label", "Seguir");
                    this.style.backgroundColor = "rgb(0, 0, 0)";
                }
            });

            var i_0 = document.createElement('i');
            i_0.className = "mdi mdi-radar ";
            i_0.style.fontSize = "2em";
            div_6.appendChild(i_0);

            div_4.appendChild(div_6);

            div_3.appendChild(div_4);

            div_2.appendChild(div_3);
        }
        pai.appendChild(div_2);

    }

    this.montaProjetos = function (pai) {
        var nomeProjeto = ["PT #510 - LIBERAÇÃO UBER", "PT #123 - LIBERAÇÃO DO ABORTO"];
        var descricaoProjeto = ["Lei constitucional que prevê a proibição do aplicativo uber na cidade de São Paulo.", "Lei constitucional que prevê a proibição do aplicativo uber na cidade de São Paulo."];


        var col = document.createElement('div');
        col.className = "col s12";

        var ul = document.createElement('ul');
        ul.className = "collapsible";
        ul.setAttribute("data-collapsible", "accordion");
        for (var j = 0; j < nomeProjeto.length; j++) {
            var li = document.createElement('li');
            //header
            var divHeader = document.createElement('div');
            divHeader.className = "collapsible-header pointer row grey lighten-2";
            divHeader.style.minHeight = "4em";
            divHeader.style.padding = "1em 0em";
            divHeader.style.borderBottom = "1px solid #cccccc";

            var div_3 = document.createElement('div');
            div_3.className = "col s12 montserrat-bold";
            div_3.appendChild(document.createTextNode("Projeto"));
            divHeader.appendChild(div_3);


            var div_4 = document.createElement('div');
            div_4.className = "col s12 grey-text";
            div_4.appendChild(document.createTextNode(nomeProjeto[j]));
            divHeader.appendChild(div_4);

            li.appendChild(divHeader);
            //--header
            //body
            var divBody = document.createElement('div');
            divBody.className = "collapsible-body pointer";

            var div_5 = document.createElement('div');
            div_5.className = "row grey-lighten-4";
            div_5.style.marginBottom = 0;
            div_5.style.borderBottom = "1px solid #cccccc";
            div_5.style.minHeight = "4em";
            div_5.style.padding = "1em 0em";

            var div_6 = document.createElement('div');
            div_6.className = "col s12 montserrat-bold";
            div_6.appendChild(document.createTextNode("Descrição"));
            div_5.appendChild(div_6);
            li.appendChild(divBody);

            var div_7 = document.createElement('div');
            div_7.className = "col s12 grey-text";
            div_7.appendChild(document.createTextNode(descricaoProjeto[j]));
            div_5.appendChild(div_7);
            divBody.appendChild(div_5);

            li.appendChild(divBody);
            //--body
            ul.appendChild(li);
        }

        col.appendChild(ul);
        pai.appendChild(col);
        $('.collapsible').collapsible();


    }




    getTitle("Oconselho - Projetos de Lei");

    this.construtor();
}