function PagPopularidade() {

    this.construtor = function () {
        this.montaPagHtml();
    }

    this.montaPagHtml = function () {
        pagina.getMain().empty();


        var nomePoliticos = ["Michel", "Guilherme", "Gustavo"];
        var urlPoliticos = ["img/img1.jpg", "img/img2.jpg", "img/img3.jpg"];
        var partidoPoliticos = ["pcb", "pt", "pco"];
        var bens = [20000, 90000, 160000];
        var idPolitico = "129022";
        var div_0 = document.createElement('div');
        div_0.style.marginTop = "4.5em";
        div_0.className = "row";

        var div_1 = document.createElement('div');
        div_1.className = "col s12";

        var ul_0 = document.createElement('ul');
        ul_0.className = "tabs";

        var li_0 = document.createElement('li');
        li_0.className = "tab col s3 montserrat-bold";

        var a_0 = document.createElement('a');
        a_0.href = "#prefeitos";
        a_0.className = "active";
        a_0.appendChild(document.createTextNode("Prefeitos"));
        li_0.appendChild(a_0);

        ul_0.appendChild(li_0);


        div_1.appendChild(ul_0);

        div_0.appendChild(div_1);


        var div_prefeitos = document.createElement('div');
        div_prefeitos.id = "prefeitos";
        div_prefeitos.className = "col s12";
        div_prefeitos.style.marginTop = "1em";


        this.montaCarrossel(div_prefeitos);

        div_0.appendChild(div_prefeitos);

        pagina.getMain().appendChild(div_0);



        var div_15 = document.createElement('div');
        div_15.className = "container";

        var div_nomePolitico = document.createElement('div');
        div_nomePolitico.className = "center";
        div_nomePolitico.id = "nomePolitico";

        var span_0 = document.createElement('span');
        span_0.className = "montserrat-bold";
        span_0.appendChild(document.createTextNode("Pessoa1"));
        div_nomePolitico.appendChild(span_0);

        div_15.appendChild(div_nomePolitico);


        var div_partidoPolitico = document.createElement('div');
        div_partidoPolitico.id = "partidoPolitico";
        div_partidoPolitico.className = "center";

        var span_1 = document.createElement('span');
        span_1.className = "montserrat";
        span_1.appendChild(document.createTextNode("20 PSOL"));
        div_partidoPolitico.appendChild(span_1);

        div_15.appendChild(div_partidoPolitico);


        var div_16 = document.createElement('div');
        div_16.className = "center";

        var h5_0 = document.createElement('h5');
        h5_0.className = "montserrat-bold";
        h5_0.appendChild(document.createTextNode("Popularidade:"));
        div_16.appendChild(h5_0);

        div_15.appendChild(div_16);


        var div_17 = document.createElement('div');

        var div_dadosPolitico = document.createElement('div');
        div_dadosPolitico.className = "col s12 center";
        div_dadosPolitico.id = "dadosPolitico";
        div_17.appendChild(div_dadosPolitico);


        //        var div_18 = document.createElement('div');
        //        div_18.className = "chart-container";
        //        div_18.style.display = "flex";
        //        div_18.style.justifyContent = "center";
        //        div_18.style.marginTop = "-1.8em";
        //        div_18.style.marginBottom = "1em";
        //
        //        var div_d3SimpleInteraction = document.createElement('div');
        //        div_d3SimpleInteraction.style.width = "12em";
        //        div_d3SimpleInteraction.id = "d3-simple-interaction-2";
        //        div_d3SimpleInteraction.className = "chart";
        //        div_18.appendChild(div_d3SimpleInteraction);
        //
        //        div_17.appendChild(div_18);


        var div_19 = document.createElement('div');
        div_19.className = "center";

        var div_20 = document.createElement('div');
        div_20.className = "fb-share-button";

        var a_2 = document.createElement('a');
        a_2.className = "fb-xfbml-parse-ignore";
        a_2.href = "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&src=sdkpreparse";
        a_2.target = "_blank";
        a_2.appendChild(document.createTextNode("Compartilhar"));
        div_20.appendChild(a_2);

        div_19.appendChild(div_20);

        div_17.appendChild(div_19);

        div_15.appendChild(div_17);

        pagina.getMain().appendChild(div_15);




        bens.sort(function (a, b) {
            return b - a
        });
        $('ul.tabs').tabs();
        var elem = document.querySelector('.carousel');
        var flkty = new Flickity(elem);
        politico = flkty.selectedElement;
        var wsUrl = "http://oconselho-aoub.rhcloud.com/api/";
        var hq = new HttpReq("POST", wsUrl + "popularidade/129022/", function (res) {
            res = JSON.parse(res);
            document.getElementById('dadosPolitico').innerHTML = "<span class='value green-text'>" + res.positivo + "</span>" +
                "<span style='font-size:1em;display: flex;align-items: center;'>%</span>" +
                "<span style='font-size:1em;display: flex;align-items: center;margin:0em 0.5em;'>|</span>" +
                "<span class='value red-text'>" + res.negativo + "</span> <span style='font-size:1em;display: flex;align-items: center;'>%</span>";
        });
        hq.enviar();

        document.getElementById('nomePolitico').innerHTML = "<span class='montserrat-bold'>" + politico.getAttribute("nomePolitico") + "</span>";
        document.getElementById('partidoPolitico').innerHTML = "<span class='montserrat'>" + politico.getAttribute("partidoPolitico") + "</span>";

        montaGrafico2(flkty, bens);
        return true;

    }

    this.montaCarrossel = function (pai) {
        var nomePoliticos = ["Michel", "Guilherme", "Gustavo"];
        var urlPoliticos = ["img/img1.jpg", "img/img2.jpg", "img/img3.jpg"];
        var partidoPoliticos = ["pcb", "pt", "pco"];
        var bens = [20000, 90000, 160000];
        var div_2 = document.createElement('div');
        div_2.className = "carousel";

        for (i = 0; i < nomePoliticos.length; i++) {
            var div_3 = document.createElement('div');
            div_3.className = "carousel-cell";
            div_3.setAttribute("partidopolitico", "20 PSOL");
            div_3.setAttribute("value", bens[i]);
            div_3.setAttribute("nomepolitico", nomePoliticos[i]);
            div_3.style.backgroundImage = "url(" + urlPoliticos[i] + ")";

            var div_4 = document.createElement('div');
            div_4.className = "right";
            div_4.style.display = "flex";
            div_4.style.flexDirection = "column";

            var img_0 = document.createElement('img');
            img_0.style.width = "2.5em";
            img_0.src = "img/partidos/" + partidoPoliticos[i] + ".png";
            img_0.className = "right";
            div_4.appendChild(img_0);


            var div_5 = document.createElement('div');
            div_5.style.height = "8.5em";
            div_4.appendChild(div_5);


            var div_6 = document.createElement('div');
            div_6.style.width = "3em";
            div_6.style.height = "3em";
            div_6.style.borderRadius = "30em";
            div_6.style.display = "flex";
            div_6.style.justifyContent = "center";
            div_6.style.alignItems = "center";
            div_6.style.top = "14em";
            div_6.className = "black white-text";

            var i_0 = document.createElement('i');
            i_0.className = "mdi mdi-radar ";
            i_0.style.fontSize = "2em";
            div_6.appendChild(i_0);

            div_4.appendChild(div_6);

            div_3.appendChild(div_4);

            div_2.appendChild(div_3);
        }
        pai.appendChild(div_2);

    }
    getTitle("Oconselho - Popularidade");

    this.construtor();
}