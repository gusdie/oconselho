function PagBens() {

    this.construtor = function () {
        this.montaPagHtml();
    }

    this.montaPagHtml = function () {
        pagina.getMain().empty();


        var bens = [20000, 90000, 160000, 20000, 90000, 160000, 20000, 90000, 160000, 160000];
        var div_0 = document.createElement('div');
        div_0.style.marginTop = "3.6em";
        div_0.className = "row";

        var div_1 = document.createElement('div');
        div_1.className = "col s12";

        var ul_0 = document.createElement('ul');
        ul_0.className = "tabs";

        var li_0 = document.createElement('li');
        li_0.className = "tab col s3 montserrat-bold";

        var a_0 = document.createElement('a');
        a_0.href = "#prefeitos";
        a_0.className = "active";
        a_0.addEventListener("click", function () {
            pag.montaCarrossel("prefeitos", div_prefeitos);
        });
        a_0.appendChild(document.createTextNode("Prefeitos"));
        li_0.appendChild(a_0);

        ul_0.appendChild(li_0);


        var li_1 = document.createElement('li');
        li_1.className = "tab col s3 montserrat-bold";

        var a_1 = document.createElement('a');
        a_1.href = "#vereadores";
        a_1.addEventListener("click", function () {
            pag.montaCarrossel("vereadores", div_vereadores);
        });
        a_1.appendChild(document.createTextNode("Vereadores"));
        li_1.appendChild(a_1);

        //ul_0.appendChild(li_1);

        div_1.appendChild(ul_0);

        div_0.appendChild(div_1);


        var div_prefeitos = document.createElement('div');
        div_prefeitos.id = "prefeitos";
        div_prefeitos.className = "col s12";
        div_prefeitos.style.marginTop = "1em";

        div_0.appendChild(div_prefeitos);


        var div_vereadores = document.createElement('div');
        div_vereadores.id = "vereadores";
        div_vereadores.className = "col s12";
        div_vereadores.style.marginTop = "1em";



        div_0.appendChild(div_vereadores);

        pagina.getMain().appendChild(div_0);


        var div_15 = document.createElement('div');
        div_15.className = "container";

        var div_nomePolitico = document.createElement('div');
        div_nomePolitico.className = "center";
        div_nomePolitico.id = "nomePolitico";

        var span_0 = document.createElement('span');
        span_0.className = "montserrat-bold";
        span_0.appendChild(document.createTextNode(""));
        div_nomePolitico.appendChild(span_0);

        div_15.appendChild(div_nomePolitico);


        var div_partidoPolitico = document.createElement('div');
        div_partidoPolitico.id = "partidoPolitico";
        div_partidoPolitico.className = "center";

        var span_1 = document.createElement('span');
        span_1.className = "montserrat";
        span_1.appendChild(document.createTextNode(""));
        div_partidoPolitico.appendChild(span_1);

        div_15.appendChild(div_partidoPolitico);


        var div_16 = document.createElement('div');
        div_16.className = "center";

        var h5_0 = document.createElement('h5');
        h5_0.className = "montserrat-bold";
        h5_0.appendChild(document.createTextNode("Bens:"));
        div_16.appendChild(h5_0);

        div_15.appendChild(div_16);


        var div_17 = document.createElement('div');

        var div_dadosPolitico = document.createElement('div');
        div_dadosPolitico.className = "col s12 center";
        div_dadosPolitico.id = "dadosPolitico";
        div_17.appendChild(div_dadosPolitico);


        //        var div_18 = document.createElement('div');
        //        div_18.className = "chart-container";
        //        div_18.style.display = "flex";
        //        div_18.style.justifyContent = "center";
        //        div_18.style.marginTop = "-1.8em";
        //        div_18.style.marginBottom = "1em";
        //
        //        var div_d3SimpleInteraction = document.createElement('div');
        //        div_d3SimpleInteraction.style.width = "12em";
        //        div_d3SimpleInteraction.id = "d3-simple-interaction";
        //        div_d3SimpleInteraction.className = "chart";
        //        div_18.appendChild(div_d3SimpleInteraction);
        //
        //        div_17.appendChild(div_18);




        div_15.appendChild(div_17);

        pagina.getMain().appendChild(div_15);


        this.montaCarrossel("prefeitos", div_prefeitos);

        $('ul.tabs').tabs();

        return true;

    }

    this.montaCarrossel = function (tipo, pai) {
        if (tipo == "prefeitos") {
            getPoliticos = "politico/consultaPrefeitos";
        } else {
            getPoliticos = "politico/consultaVereadores";
        }
        var hr = new HttpReq("POST", pagina.apiUrl() + getPoliticos, function (res) {
            res = JSON.parse(res);
            console.log(res);
            var bens = [];
            var div_2 = document.createElement('div');
            div_2.className = "carousel";

            for (i = 0; i < res.result.length; i++) {
                var div_3 = document.createElement('div');
                div_3.className = "carousel-cell";
                div_3.setAttribute("partidopolitico", res.result[i].partidoSigla);
                var hq = new HttpReq("POST", pagina.apiUrl() + "bens/recuperarTotalDePoliticoComId/", function (obj) {
                    obj = JSON.parse(obj);
                    bens.push(parseInt(obj.result));
                });
                hq.enviar("idTse=" + res.result[i].idTse);
                div_3.setAttribute("_id", res.result[i]._id);
                div_3.setAttribute("nomepolitico", res.result[i].apelido);
                div_3.style.backgroundImage = "url(" + res.result[i].imagem + ")";

                var div_4 = document.createElement('div');
                div_4.className = "right";
                div_4.style.display = "flex";
                div_4.style.flexDirection = "column";

                var span = document.createElement("span");
                span.setAttribute("aria-label", "Partido");
                span.className = "hint--top";
                var img_0 = document.createElement('img');
                img_0.style.width = "2.5em";
                img_0.src = "img/partidos/" + res.result[i].partidoSigla + ".png";
                img_0.className = "right";
                span.appendChild(img_0);
                div_4.appendChild(span);


                var div_5 = document.createElement('div');
                div_5.style.height = "11.5em";
                div_4.appendChild(div_5);


                var div_6 = document.createElement('div');
                div_6.style.width = "3em";
                div_6.style.height = "3em";
                div_6.setAttribute("aria-label", "Seguir");
                div_6.style.borderRadius = "30em";
                div_6.style.left = "0.8em";
                div_6.style.display = "flex";
                div_6.style.justifyContent = "center";
                div_6.style.alignItems = "center";
                div_6.className = "white-text hint--top";
                div_6.style.backgroundColor = "#000000";
                div_6.addEventListener("click", function (e) {
                    if (this.style.backgroundColor == "rgb(0, 0, 0)") {
                        var hq = new HttpReq("POST", pagina.apiUrl() + "seguindo/cadastrar", function (res) {
                            $("#modalSeguir").openModal();
                            //this.setAttribute("aria-label", "Seguindo");
                            //this.style.backgroundColor = "#346A66";
                        });
                        hq.enviar("idPolitico=" + politico.getAttribute("id") + "&ativo=1&idUsuario=1042949799159428");

                    } else {
                        this.setAttribute("aria-label", "Seguir");
                        this.style.backgroundColor = "rgb(0, 0, 0)";
                    }
                });

                var i_0 = document.createElement('i');
                i_0.className = "mdi mdi-radar ";
                i_0.style.fontSize = "2em";
                div_6.appendChild(i_0);

                div_4.appendChild(div_6);

                div_3.appendChild(div_4);

                div_2.appendChild(div_3);
            }
            pai.appendChild(div_2);
            var elem = document.querySelector('.carousel');
            var flkty = new Flickity(elem);
            politico = flkty.selectedElement;
            document.getElementById('dadosPolitico').innerHTML = "<span class='rs montserrat'></span> <span class = 'value'></span>";
            document.getElementById('nomePolitico').innerHTML = "<span class='montserrat-bold'>" + politico.getAttribute("nomePolitico") + "</span>";
            document.getElementById('partidoPolitico').innerHTML = "<span class='montserrat'>" + politico.getAttribute("partidoPolitico") + "</span>";
            //montaGrafico(flkty, bens);
            flkty.on('select', function () {
                bens[4] = "8000000";
                politico = flkty.selectedElement;
                document.getElementById('dadosPolitico').innerHTML = "<span class='rs montserrat'>R$</span> <span class='value'>" + bens[flkty.selectedIndex] + "</span>";
                document.getElementById('nomePolitico').innerHTML = "<span class='montserrat-bold'>" + politico.getAttribute("nomePolitico") + "</span>";
                document.getElementById('partidoPolitico').innerHTML = "<span class='montserrat'>" + politico.getAttribute("partidoPolitico") + "</span>";
            });
        });
        hr.enviar();

    }
    getTitle("Oconselho - Bens");

    this.construtor();
}