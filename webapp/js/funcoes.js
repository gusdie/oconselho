window.addEventListener("load", function () {
    if (typeof document.getElementsByTagName("main")[0].empty != typeof Function) {
        Element.prototype.empty = function () {
            while (this.firstChild) this.removeChild(this.firstChild);
        }
        console.log("Element.empty() criado manualmente.");
    }

    pagina = new Pagina();

    carregando.fechar();
    pagina.abrirPaginas("Login");
});
funcionalidadesDisponiveis();
carregando = new Carregando();
alerta = new Alerta();


function verificaBuscar() {
    if (document.getElementById('iconBusca').className == "mdi mdi-magnify") {
        document.getElementById('iconBusca').className = "mdi mdi-chevron-up";
        document.getElementById('nav-search').style.display = "block";
    } else {
        document.getElementById('iconBusca').className = "mdi mdi-magnify";
        document.getElementById('nav-search').style.display = "none";
    }


}