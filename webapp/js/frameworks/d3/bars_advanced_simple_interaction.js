/* ------------------------------------------------------------------------------
 *
 *  # D3.js - bars with simple interaction
 *
 *  Demo d3.js bar chart setup with animated data source change
 *
 *  Version: 1.0
 *  Latest update: August 1, 2015
 *
 * ---------------------------------------------------------------------------- */


function montaGrafico(flkty, bens) {


    // Initialize chart
    interaction('#d3-simple-interaction', 150);
    


    // Chart setup
    function interaction(element, height) {


        // Basic setup
        // ------------------------------

        // Define main variables
        var d3Container = d3.select(element),
            margin = {
                top: 5,
                right: 20,
                bottom: 20,
                left: 10
            },
            width = d3Container.node().getBoundingClientRect().width - margin.left - margin.right,
            height = height - margin.top - margin.bottom - 5;

        // Demo data set
        var dataset = bens;



        // Construct scales
        // ------------------------------

        // Horizontal
        var x = d3.scale.ordinal()
            .domain(d3.range(dataset.length))
            .rangeRoundBands([0, width], 0.05);

        // Vertical
        var y = d3.scale.linear()
            .domain([0, d3.max(dataset)])
            .range([0, height]);


        // Create axes
        // ------------------------------

        // Horizontal
        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom");



        // Create chart
        // ------------------------------

        // Add SVG element
        var container = d3Container.append("svg");

        // Add SVG group
        var svg = container
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");



        // Add tooltip
        // ------------------------------

        // Create tooltip
        var tip = d3.tip()
            .attr('class', 'd3-tip')
            .offset([-10, 0])
            .html(function (d) {
                return d
            })

        // Initialize tooltip
        svg.call(tip);


        //
        // Append chart elements
        //

        // Append bars
        // ------------------------------

        // Add bars
        var drawBars = svg.selectAll(".d3-bar")
            .data(dataset)
            .enter()
            .append("rect")
            .attr("class", "d3-bar")
            .attr("x", function (d, i) {
                return x(i)
            })
            .attr("width", x.rangeBand())
            .attr("height", 0)
            .attr("y", height)
            .attr("fill", "#BABABA")

        // Add bar transition
        drawBars.transition()
            .delay(200)
            .duration(1000)
            .attr("height", function (d) {
                return y(d)
            })
            .attr("y", function (d) {
                return height - y(d)
            })



        // Create axes
        // ------------------------------

        // Horizontal
        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom");



        // Change data sets
        // ------------------------------


        flkty.on('select', function () {
            politico = flkty.selectedElement;
            document.getElementById('dadosPolitico').innerHTML = "<span class='rs montserrat'>R$</span> <span class='value'>" + politico.getAttribute("value") + "</span>";
            document.getElementById('nomePolitico').innerHTML = "<span class='montserrat-bold'>" + politico.getAttribute("nomePolitico") + "</span>";
            document.getElementById('partidoPolitico').innerHTML = "<span class='montserrat'>" + politico.getAttribute("partidoPolitico") + "</span>";

            // Update all rects
            svg.selectAll("rect")
                .data(dataset)
                .transition()
                .delay(0)
                .duration(1000)
                .ease('cubic-in-out')
                .attr("fill", function (d) {

                    if (d == parseInt(politico.getAttribute("value"))) {
                        return "#90c664";
                    } else {
                        return "#BABABA";
                    }
                })
                .attr("y", function (d) {
                    return height - y(d)
                })
                .attr("height", function (d) {
                    return y(d)
                })
        });


        politico = flkty.selectedElement;
        document.getElementById('dadosPolitico').innerHTML = "<span class='rs montserrat'>R$</span> <span class='value'>" + politico.getAttribute("value") + "</span>";
        document.getElementById('nomePolitico').innerHTML = "<span class='montserrat-bold'>" + politico.getAttribute("nomePolitico") + "</span>";
        document.getElementById('partidoPolitico').innerHTML = "<span class='montserrat'>" + politico.getAttribute("partidoPolitico") + "</span>";

        // Update all rects
        svg.selectAll("rect")
            .data(dataset)
            .transition()
            .delay(0)
            .duration(1000)
            .ease('cubic-in-out')
            .attr("fill", function (d) {

                if (d == parseInt(politico.getAttribute("value"))) {
                    return "#90c664";
                } else {
                    return "#BABABA";
                }
            })
            .attr("y", function (d) {
                return height - y(d)
            })
            .attr("height", function (d) {
                return y(d)
            })
            // Resize chart
            // ------------------------------

        // Call function on window resize
        $(window).on('resize', resize);

        // Call function on sidebar width change
        $('.sidebar-control').on('click', resize);

        // Resize function
        // 
        // Since D3 doesn't support SVG resize by default,
        // we need to manually specify parts of the graph that need to 
        // be updated on window resize
        function resize() {

            // Layout variables
            width = d3Container.node().getBoundingClientRect().width - margin.left - margin.right;


            // Layout
            // -------------------------

            // Main svg width
            container.attr("width", width + margin.left + margin.right);

            // Width of appended group
            svg.attr("width", width + margin.left + margin.right);


            // Axes
            // -------------------------

            // Horizontal range
            x.rangeRoundBands([0, width], 0.05);

            // Horizontal axis
            svg.selectAll('.d3-axis-horizontal').call(xAxis);


            // Chart elements
            // -------------------------

            // Bars
            svg.selectAll('.d3-bar').attr("x", function (d, i) {
                return x(i)
            }).attr("width", x.rangeBand());


        }
    }
}

function montaGrafico2(flkty, bens) {


    // Initialize chart
interaction('#d3-simple-interaction-2', 150);
    


    // Chart setup
    function interaction(element, height) {


        // Basic setup
        // ------------------------------

        // Define main variables
        var d3Container = d3.select(element),
            margin = {
                top: 5,
                right: 20,
                bottom: 20,
                left: 10
            },
            width = d3Container.node().getBoundingClientRect().width - margin.left - margin.right,
            height = height - margin.top - margin.bottom - 5;

        // Demo data set
        var dataset = bens;



        // Construct scales
        // ------------------------------

        // Horizontal
        var x = d3.scale.ordinal()
            .domain(d3.range(dataset.length))
            .rangeRoundBands([0, width], 0.05);

        // Vertical
        var y = d3.scale.linear()
            .domain([0, d3.max(dataset)])
            .range([0, height]);


        // Create axes
        // ------------------------------

        // Horizontal
        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom");



        // Create chart
        // ------------------------------

        // Add SVG element
        var container = d3Container.append("svg");

        // Add SVG group
        var svg = container
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");



        // Add tooltip
        // ------------------------------

        // Create tooltip
        var tip = d3.tip()
            .attr('class', 'd3-tip')
            .offset([-10, 0])
            .html(function (d) {
                return d
            })

        // Initialize tooltip
        svg.call(tip);


        //
        // Append chart elements
        //

        // Append bars
        // ------------------------------

        // Add bars
        var drawBars = svg.selectAll(".d3-bar")
            .data(dataset)
            .enter()
            .append("rect")
            .attr("class", "d3-bar")
            .attr("x", function (d, i) {
                return x(i)
            })
            .attr("width", x.rangeBand())
            .attr("height", 0)
            .attr("y", height)
            .attr("fill", "#BABABA")

        // Add bar transition
        drawBars.transition()
            .delay(200)
            .duration(1000)
            .attr("height", function (d) {
                return y(d)
            })
            .attr("y", function (d) {
                return height - y(d)
            })



        // Create axes
        // ------------------------------

        // Horizontal
        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom");



        // Change data sets
        // ------------------------------


        flkty.on('select', function () {
            politico = flkty.selectedElement;
            document.getElementById('dadosPolitico').innerHTML = "<span class='rs montserrat'>R$</span> <span class='value'>" + politico.getAttribute("value") + "</span>";
            document.getElementById('nomePolitico').innerHTML = "<span class='montserrat-bold'>" + politico.getAttribute("nomePolitico") + "</span>";
            document.getElementById('partidoPolitico').innerHTML = "<span class='montserrat'>" + politico.getAttribute("partidoPolitico") + "</span>";

            // Update all rects
            svg.selectAll("rect")
                .data(dataset)
                .transition()
                .delay(0)
                .duration(1000)
                .ease('cubic-in-out')
                .attr("fill", function (d) {

                    if (d == parseInt(politico.getAttribute("value"))) {
                        return "#90c664";
                    } else {
                        return "#BABABA";
                    }
                })
                .attr("y", function (d) {
                    return height - y(d)
                })
                .attr("height", function (d) {
                    return y(d)
                })
        });


        politico = flkty.selectedElement;
        document.getElementById('dadosPolitico').innerHTML = "<span class='rs montserrat'>R$</span> <span class='value'>" + politico.getAttribute("value") + "</span>";
        document.getElementById('nomePolitico').innerHTML = "<span class='montserrat-bold'>" + politico.getAttribute("nomePolitico") + "</span>";
        document.getElementById('partidoPolitico').innerHTML = "<span class='montserrat'>" + politico.getAttribute("partidoPolitico") + "</span>";

        // Update all rects
        svg.selectAll("rect")
            .data(dataset)
            .transition()
            .delay(0)
            .duration(1000)
            .ease('cubic-in-out')
            .attr("fill", function (d) {

                if (d == parseInt(politico.getAttribute("value"))) {
                    return "#90c664";
                } else {
                    return "#BABABA";
                }
            })
            .attr("y", function (d) {
                return height - y(d)
            })
            .attr("height", function (d) {
                return y(d)
            })
            // Resize chart
            // ------------------------------

        // Call function on window resize
        $(window).on('resize', resize);

        // Call function on sidebar width change
        $('.sidebar-control').on('click', resize);

        // Resize function
        // 
        // Since D3 doesn't support SVG resize by default,
        // we need to manually specify parts of the graph that need to 
        // be updated on window resize
        function resize() {

            // Layout variables
            width = d3Container.node().getBoundingClientRect().width - margin.left - margin.right;


            // Layout
            // -------------------------

            // Main svg width
            container.attr("width", width + margin.left + margin.right);

            // Width of appended group
            svg.attr("width", width + margin.left + margin.right);


            // Axes
            // -------------------------

            // Horizontal range
            x.rangeRoundBands([0, width], 0.05);

            // Horizontal axis
            svg.selectAll('.d3-axis-horizontal').call(xAxis);


            // Chart elements
            // -------------------------

            // Bars
            svg.selectAll('.d3-bar').attr("x", function (d, i) {
                return x(i)
            }).attr("width", x.rangeBand());


        }
    }
}